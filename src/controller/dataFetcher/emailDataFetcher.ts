import {Configuration} from "../../model/model";
import {AuthenticationController} from "../AuthenticationController";
import * as request from "request";
import {Response} from "request";
import {Email} from "../../model/props";

export class EmailComponent {
    constructor(private config: Configuration) {
    }
    async  getEmailData(): Promise<Email[][]> {
        let options: any = {
        headers:
            {
                Connection: 'keep-alive',
                Host: this.config.backendURL,
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + AuthenticationController.getAuthenticationToken(),
            },
        body: "",
        json: true,
        method: 'POST',
        host: this.config.backendURL
    };

    return new Promise<Email[][]>((resolve, reject) => {

        request.post(`http://${this.config.backendURL}/api/v1/get_mail_werte`, options, (error: any, response: Response, body: any) => {
            if (error) {
                reject(error);
            } else if (response.statusCode >= 300) {
                if (response.statusCode === 401) {
                    reject("Not authorized to access resource");
                }
                reject("Server responded with: " + response.statusCode + " " + body.message);
            } else {
                //console.log(body.data)
                resolve([body.data]);
            }
        });
    });
    }

    async  saveEmailData(dataRequest: Email): Promise<any> {

        let options: any = {
        headers:
            {
                Connection: 'keep-alive',
                Host: this.config.backendURL,
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + AuthenticationController.getAuthenticationToken(),
            },
        body: dataRequest,
        json: true,
        method: 'POST',
        host: this.config.backendURL
    };

    return new Promise<any>((resolve, reject) => {

        request.post(`http://${this.config.backendURL}/api/v1/update_space`, options, (error: any, response: Response, body: any) => {
            if (error) {
                reject(error);
            } else if (response.statusCode >= 300) {
                if (response.statusCode === 401) {
                    reject("Not authorized to access resource");
                }
                reject("Server responded with: " + response.statusCode + " " + body.message);
            } else {
                //console.log(body.data)
                resolve(body);
            }
        });
    });
    }
    async  addEmailSpace(dataRequest: Email): Promise<any> {
        let options: any = {
        headers:
            {
                Connection: 'keep-alive',
                Host: this.config.backendURL,
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + AuthenticationController.getAuthenticationToken(),
            },
        body: dataRequest,
        json: true,
        method: 'POST',
        host: this.config.backendURL
    };

    return new Promise<any>((resolve, reject) => {

        request.post(`http://${this.config.backendURL}/api/v1/add_space_to_mail`, options, (error: any, response: Response, body: any) => {
            if (error) {
                reject(error);
            } else if (response.statusCode >= 300) {
                if (response.statusCode === 401) {
                    reject("Not authorized to access resource");
                }
                reject("Server responded with: " + response.statusCode + " " + body.message);
            } else {
                //console.log(body.data)
                resolve(body);
            }
        });
    });
    }
    async  deleteEmailSpace(dataRequest: {space:string}): Promise<any> {
        let options: any = {
        headers:
            {
                Connection: 'keep-alive',
                Host: this.config.backendURL,
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + AuthenticationController.getAuthenticationToken(),
            },
        body: dataRequest,
        json: true,
        method: 'POST',
        host: this.config.backendURL
    };

    return new Promise<any>((resolve, reject) => {

        request.post(`http://${this.config.backendURL}/api/v1/delete_space_from_mail`, options, (error: any, response: Response, body: any) => {
            if (error) {
                reject(error);
            } else if (response.statusCode >= 300) {
                if (response.statusCode === 401) {
                    reject("Not authorized to access resource");
                }
                reject("Server responded with: " + response.statusCode + " " + body.message);
            } else {
                //console.log(body.data)
                resolve(body);
            }
        });
    });
    }

    async  updateEmailSkipDates(dataRequest: {space_id: number,skip_dates:string[]}): Promise<any> {

        let options: any = {
        headers:
            {
                Connection: 'keep-alive',
                Host: this.config.backendURL,
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + AuthenticationController.getAuthenticationToken(),
            },
        body: dataRequest,
        json: true,
        method: 'POST',
        host: this.config.backendURL
    };

    return new Promise<any>((resolve, reject) => {

        request.post(`http://${this.config.backendURL}/api/v1/mails/update_skip_dates`, options, (error: any, response: Response, body: any) => {
            if (error) {
                reject(error);
            } else if (response.statusCode >= 300) {
                if (response.statusCode === 401) {
                    reject("Not authorized to access resource");
                }
                reject("Server responded with: " + response.statusCode + " " + body.message);
            } else {
                //console.log(body.data)
                resolve(body);
            }
        });
    });
    }

    async  getEmailSkipDates(dataRequest: {space_id: number}): Promise<any> {

        let options: any = {
        headers:
            {
                Connection: 'keep-alive',
                Host: this.config.backendURL,
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + AuthenticationController.getAuthenticationToken(),
            },
        body: dataRequest,
        json: true,
        method: 'POST',
        host: this.config.backendURL
    };

    return new Promise<any>((resolve, reject) => {

        request.post(`http://${this.config.backendURL}/api/v1/mails/get_skip_dates`, options, (error: any, response: Response, body: any) => {
            if (error) {
                reject(error);
            } else if (response.statusCode >= 300) {
                if (response.statusCode === 401) {
                    reject("Not authorized to access resource");
                }
                reject("Server responded with: " + response.statusCode + " " + body.message);
            } else {
                //console.log(body.data)
                resolve(body);
            }
        });
    });
    }
}
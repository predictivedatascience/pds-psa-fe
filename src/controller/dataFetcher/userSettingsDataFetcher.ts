import {Configuration} from "../../model/model";
import * as request from "request";
import {Response} from "request";
import {change_password, SearchUserRequestDTO, UserRoleDTO, add_user, del_user, AuthResult,} from "../../model/dtos";
import {AuthenticationController} from "../AuthenticationController";

export class UserSettingsDataFetcher {

    constructor(private config: Configuration) {
    }

    async tryLogin(username: string, pwd: string): Promise<AuthResult> {

        let options: any = {
            headers: {
                Connection: 'keep-alive',
                Host: this.config.backendURL,
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: {
                'name': username,
                'password': pwd
            },
            json: true,
            method: 'POST',
            host: this.config.backendURL
        };

        return new Promise<AuthResult>((resolve, reject) => {
            request.post(`http://${this.config.backendURL}/api/v1/auth`, options, (error: any, response: Response, body: any) => {
                if (error) {
                    reject(error);
                }
                else {
                    if (response.statusCode === 200) {
                        resolve({code: response.statusCode, token: body.data.access_token});
                    } else {
                        resolve({code: response.statusCode, message: body.message});
                    }
                }
            });
        });
    }
    async evalLogin(authToken: string): Promise<AuthResult> {
        let options: any = {
            headers: {
                Connection: 'keep-alive',
                Host: this.config.backendURL,
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: `Bearer ${authToken}`,
            },
            json: true,
            method: 'GET',
            host: this.config.backendURL
        };

        return new Promise<AuthResult>((resolve, reject) => {
            request.get(`http://${this.config.backendURL}/api/v1/check_auth`, options, (error: any, response: Response, body: any) => {
                if (error) {
                    reject(error);
                }
                else {
                    resolve({code: response.statusCode, token: authToken});
                }
            });
        });

    }

    async add_user(username: string, pwd: string, UserRole: string): Promise<add_user> {

        let options: any = {
            headers: {
                Connection: 'keep-alive',
                Host: this.config.backendURL,
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: {
                'name': username,
                'password': pwd,
                'UserRole': UserRole
            },
            json: true,
            method: 'POST',
            host: this.config.backendURL
        };

        return new Promise<add_user>((resolve, reject) => {
            request.post(`http://${this.config.backendURL}/api/v1/add_user`, options, (error: any, response: Response, body: any) => {
                if (error) {
                    reject(error);
                }
                else {
                    if (response.statusCode === 90) {
                        resolve({code: response.statusCode});
                    } else {
                        resolve({code: response.statusCode, message: body.message});
                    }
                }
            });
        });
    }

    async changePassword(username: string, newPassword: string): Promise<change_password> {

        let options: any = {
            headers: {
                Connection: 'keep-alive',
                Host: this.config.backendURL,
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + AuthenticationController.getAuthenticationToken(),
            },
            body: {
                'name': username,
                'new_password': newPassword
            },
            json: true,
            method: 'POST',
            host: this.config.backendURL
        };


        return new Promise<change_password>((resolve, reject) => {
            request.post(`http://${this.config.backendURL}/api/v1/changePwd`, options, (error: any, response: Response, body: any) => {
                //console.log(response.statusCode);
                if (error) {
                    reject(error);
                }
                else {
                    if (response.statusCode === 90) {
                        resolve({code: response.statusCode});
                    } else {
                        resolve({code: response.statusCode, message: body.message});
                    }
                }
            });
        });
    }


    async del_user(username: string): Promise<del_user> {

        let options: any = {
            headers: {
                Connection: 'keep-alive',
                Host: this.config.backendURL,
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: {
                'name': username,
            },
            json: true,
            method: 'POST',
            host: this.config.backendURL
        };

        return new Promise<del_user>((resolve, reject) => {
            request.post(`http://${this.config.backendURL}/api/v1/del_user`, options, (error: any, response: Response, body: any) => {
                if (error) {
                    reject(error);
                }
                else {
                    if (response.statusCode === 80) {
                        resolve({code: response.statusCode});
                    } else {
                        resolve({code: response.statusCode, message: body.message});
                    }
                }
            });
        });
    }



    async getAllRoles(): Promise<any> {

        let options: any = {
            headers: {
                Connection: 'keep-alive',
                Host: this.config.backendURL,
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + AuthenticationController.getAuthenticationToken(),
            },
            body: '',
            json: true,
            method: 'GET',
            host: this.config.backendURL
        };

        return new Promise<any>((resolve, reject) => {
            request.get(`http://${this.config.backendURL}/api/v1/roles/search/all`, options, (error: any, response: Response, body: any) => {
                if (error) {
                    reject(error);
                } else if (response.statusCode >= 300) {
                    if (response.statusCode === 401) {
                        reject("Not authorized to access resource");
                    }
                    reject("Server responded with: " + response.statusCode + " " + body.message);
                }
                else {
                    //console.log(body.data)
                    resolve(body.data);
                }
            });
        });
    }

    async getCurrentRole(): Promise<any> {

        let options: any = {
            headers: {
                Connection: 'keep-alive',
                Host: this.config.backendURL,
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + AuthenticationController.getAuthenticationToken(),
            },
            body: '',
            json: true,
            method: 'GET',
            host: this.config.backendURL
        };

        return new Promise<any>((resolve, reject) => {
            request.get(`http://${this.config.backendURL}/api/v1/roles/search`, options, (error: any, response: Response, body: any) => {
                if (error) {
                    reject(error);
                } else if (response.statusCode >= 300) {
                    if (response.statusCode === 401) {
                        reject("Not authorized to access resource");
                    }
                    reject("Server responded with: " + response.statusCode + " " + body.message);
                }
                else {
                    //console.log(body.data)
                    resolve(body.data);
                }
            });
        });
    }

    async searchUser(dataRequest: SearchUserRequestDTO): Promise<any> {

        let options: any = {
            headers: {
                Connection: 'keep-alive',
                Host: this.config.backendURL,
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + AuthenticationController.getAuthenticationToken(),
            },
            body: dataRequest,
            json: true,
            method: 'POST',
            host: this.config.backendURL
        };

        return new Promise<any>((resolve, reject) => {
            request.post(`http://${this.config.backendURL}/api/v1/roles/search`, options, (error: any, response: Response, body: any) => {
                if (error) {
                    reject(error);
                } else if (response.statusCode >= 300) {
                    if (response.statusCode === 401) {
                        reject("Not authorized to access resource");
                    }
                    reject("Server responded with: " + response.statusCode + " " + body.message);
                }
                else {
                    //console.log(body.data)
                    resolve(body.data);
                }
            });
        });
    }

    async upsertUserWithRoles(dataRequest: UserRoleDTO): Promise<any> {

        let options: any = {
            headers: {
                Connection: 'keep-alive',
                Host: this.config.backendURL,
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + AuthenticationController.getAuthenticationToken(),
            },
            body: dataRequest,
            json: true,
            method: 'POST',
            host: this.config.backendURL
        };
        console.log(options)
        return new Promise<any>((resolve, reject) => {
            request.post(`http://${this.config.backendURL}/api/v1/roles/upsert`, options, (error: any, response: Response, body: any) => {
                if (error) {
                    reject(error);
                } else if (response.statusCode >= 300) {
                    if (response.statusCode === 401) {
                        reject("Not authorized to access resource");
                    }
                    reject("Server responded with: " + response.statusCode + " " + body.message);
                }
                else {
                    //console.log(body.data)
                    resolve(body.data);
                }
            });
        });
    }

    async deleteRolesFromUser(dataRequest: UserRoleDTO): Promise<any> {

        let options: any = {
            headers: {
                Connection: 'keep-alive',
                Host: this.config.backendURL,
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + AuthenticationController.getAuthenticationToken(),
            },
            body: dataRequest,
            json: true,
            method: 'DELETE',
            host: this.config.backendURL
        };
        console.log(options)
        return new Promise<any>((resolve, reject) => {
            request.delete(`http://${this.config.backendURL}/api/v1/roles/delete`, options, (error: any, response: Response, body: any) => {
                if (error) {
                    reject(error);
                } else if (response.statusCode >= 300) {
                    if (response.statusCode === 401) {
                        reject("Not authorized to access resource");
                    }
                    reject("Server responded with: " + response.statusCode + " " + body.message);
                }
                else {
                    //console.log(body.data)
                    resolve(body.data);
                }
            });
        });
    }
}
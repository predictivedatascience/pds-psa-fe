import {Configuration, IDataProvider} from "../../model/model";
import {Email} from "../../model/props";
import {UserSettingsDataFetcher} from './userSettingsDataFetcher'
import {EmailComponent} from "./emailDataFetcher";
import {
    add_user,
    AllUsersAndRolesResponseDTO,
    AuthResult,
    change_password,
    del_user, SearchUserRequestDTO, UpsertRoleDTO,
    UserRoleDTO
} from "../../model/dtos";
import {EmailInterface} from "../../model/email/model";

export class DataFetcher implements IDataProvider {

    private userSettings: any;
    private email: EmailInterface;

    constructor(private config: Configuration) {
        this.userSettings = new UserSettingsDataFetcher(this.config)
        this.email = new EmailComponent(this.config)
    }

    // User and Role Settings
    tryLogin(username: string, pwd: string): Promise<AuthResult> {
        return this.userSettings.tryLogin(username, pwd)
    }

    evalLogin(authToken: string): Promise<AuthResult> {
        return this.userSettings.evalLogin(authToken)
    }

    add_user(username: string, pwd: string, UserRole: string): Promise<add_user> {
        return this.userSettings.add_user(username, pwd, UserRole)
    }

    del_user(username: string): Promise<del_user> {
        return this.userSettings.del_user(username)
    }

    changePassword(username: string, newPassword: string): Promise<change_password> {
        return this.userSettings.changePassword(username, newPassword)
    }
    getAllRoles(): Promise<AllUsersAndRolesResponseDTO> {
        return this.userSettings.getAllRoles()
    }

    getCurrentRole(): Promise<any> {
        return this.userSettings.getCurrentRole()
    }

    searchUser(dataRequest: SearchUserRequestDTO): Promise<any> {
        return this.userSettings.searchUser(dataRequest)
    }
    upsertUserWithRoles(dataRequest: UpsertRoleDTO): Promise<any> {
        return this.userSettings.upsertUserWithRoles(dataRequest)
    }
    deleteRolesFromUser(dataRequest: UpsertRoleDTO): Promise<any> {
        return this.userSettings.deleteRolesFromUser(dataRequest)
    }

    // Email
    addEmailSpace(dataRequest: Email): Promise<any> {
        return this.email.addEmailSpace(dataRequest);
    }

    deleteEmailSpace(dataRequest: { space: string }): Promise<any> {
        return this.email.deleteEmailSpace(dataRequest);
    }

    getEmailData(): Promise<Email[][]> {
        return this.email.getEmailData();
    }

    getEmailSkipDates(dataRequest: { space_id: number }): Promise<any> {
        return this.email.getEmailSkipDates(dataRequest);
    }

    saveEmailData(request: Email): Promise<any> {
        return this.email.saveEmailData(request);
    }

    updateEmailSkipDates(dataRequest: { space_id: number; skip_dates: string[] }): Promise<any> {
        return this.email.updateEmailSkipDates(dataRequest);
    }

}

import { IDataProvider } from "../../../model/model";
import {
  Email,
  EmailAttachment,
  EmailsDashboardState,
} from "../../../model/props";
import {EmailInterface} from "../../../model/email/model";

/**
 * Class used to obtain data for EmailSrttingsDashboard
 */
export class EmailSettingsFactory {
   /**
     * @param dataProvider  Object utilised for communication with BE and obtaining data.
     */
  constructor(private dataProvider: EmailInterface) {}

  /**
     * Asynchronous function used to contact BE to obtain data.
     */
  async getEmailData(sort: string): Promise<EmailsDashboardState> {
    const emptyRow: Email = {
      space: "",
      subject: "",
      body: "",
      recipients: [],
      attachment: {} as EmailAttachment,
    };
    let dataResponseEmails: Email[][] = await this.dataProvider.getEmailData();
    let data: Email[] = dataResponseEmails[0];
    //console.log(dataResponseEmails);
    //console.log(dataResponseEmails);
    data.forEach((row) => {
      // @ts-ignore
      row.attachment = row.attachment[0];
      row.id = row.id!.toString();
      let usedIds: string[] = [];
      row.recipients.forEach((recipient) => {
        if (!recipient.id) {
          recipient.id = recipient.name.toLocaleLowerCase().replace(" ", "_");
        }
        if (usedIds.indexOf(recipient.id) !== -1) {
          recipient.id += usedIds.length;
        }
        usedIds.push(recipient.id);
      });
    });
    if (sort.indexOf("_asc") !== -1) {
      sort = sort.replace("_asc", "");
      data.sort((a, b) =>
          // @ts-ignore
        !Array.isArray(a[sort])
            // @ts-ignore
          ? a[sort].toString().localeCompare(b[sort].toString())
            // @ts-ignore
          : a[sort].length.toString().localeCompare(b[sort].length.toString())
      );
    } else if (sort.indexOf("_desc") !== -1) {
      sort = sort.replace("_desc", "");
      //console.log(sort);
      data.sort((a, b) =>
        // @ts-ignore
        !Array.isArray(a[sort])
            // @ts-ignore
          ? b[sort].toString().localeCompare(a[sort].toString())
            // @ts-ignore
          : b[sort].length.toString().localeCompare(a[sort].length.toString())
      );
    }
    //console.log(data);
    // @ts-ignore
    return { spaces: data, errorMsg: "", isLoading: false };
  }

   /**
     * Asynchronous function used to contact BE, to save email data
     * @param request Provides the data to be written to the DB
     */
  async saveEmailData(request: Email) {
    // @ts-ignore
    request.attachment = [request.attachment];
    //console.log({ update: request });
    let dataResponseEmails = await this.dataProvider.saveEmailData(request);
    //console.log(dataResponseEmails);
    return dataResponseEmails;
  }

  /**
     * Asynchronous function used to contact BE, to add new email space
     * @param request Provides the data to be written to the DB
     */
  async addEmailSpace(request: Email) {
    // @ts-ignore
    request.attachment = [request.attachment];
    //console.log({ insert: request });
    let dataResponseEmails = await this.dataProvider.addEmailSpace(request);
    //console.log(dataResponseEmails);
    return dataResponseEmails;
  }

  /**
     * Asynchronous function used to contact BE, to delete email space
     * @param space Provides the space id
     */
  async deleteSpace(space: string) {
    let request = { space: space };
    //console.log(request);
    let dataResponseEmails = await this.dataProvider.deleteEmailSpace(request);
    //console.log(dataResponseEmails);
    return dataResponseEmails;
  }

    /**
     * Asynchronous function used to contact BE, to update "skip dates" for mailing
     * @param space_id Provides the space id
     * @param skip_dates Provides dates to skip
     */
  async updateEmailSkipDates(space_id:number, skip_dates:string[]) {
    let request = {space_id:space_id,skip_dates: skip_dates}
    //console.log(request)
    let dataResponseSkipDates = await this.dataProvider.updateEmailSkipDates(request);
    //console.log(dataResponseSkipDates);
    return dataResponseSkipDates;
  }

   /**
     * Asynchronous function used to contact BE, to get "skip dates" for mailing
     * @param space_id Provides the space id
     */
  async getEmailSkipDates(space_id:number) {
    let request = {space_id:space_id}
    //console.log(request)
    let dataResponseSkipDates = await this.dataProvider.getEmailSkipDates(request);
    //console.log(dataResponseSkipDates);
    return dataResponseSkipDates;
  }
}

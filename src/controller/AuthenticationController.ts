import {DataFetcher} from "./dataFetcher/DataFetcher";
import {AuthResult,} from "../model/dtos";
import {UnauthenticatedException} from "../exceptions/UnauthenticatedException";

export class AuthenticationController {

    constructor(private dataFetcher: DataFetcher) {

    }

    public async tryLogin(username: string, pwd: string): Promise<AuthResult> {
        let authResult: AuthResult = await this.dataFetcher.tryLogin(username, pwd  );
        if (authResult.code === 200) {
            window.sessionStorage.setItem('sid', authResult.token ? authResult.token : '',);
            window.history.pushState('', '', '/spaces')
        } else {
            throw new UnauthenticatedException(authResult.message ? 'Cant login, check login and password.' : 'Cant login, check login and password.');
        }
        return authResult;
    }

    public static getAuthenticationToken(): string {
        let token: string | null = window.sessionStorage.getItem("sid");
        if (!token) {
            //throw new UnauthenticatedException("Invalid token. Logout and login again.");
            window.history.pushState('', 'QS Reports', '/auth/login')
            //throw new UnauthenticatedException("Invalid token. Logout and login again.");
        }
        return <string>token;
    }

    public async isUserLoggedIn(): Promise<boolean> {
        try {
            let token: string = AuthenticationController.getAuthenticationToken();
            let authResult: AuthResult = await this.dataFetcher.evalLogin(token);
            if (authResult.code !== 200) {
                return false;
            }
        } catch (e) {
            return false;
        }
        return true;
    }

    public logOut() {
        window.sessionStorage.clear();
        window.location.href = "/auth/login"; //TODO check if this is a good solution
    }
}
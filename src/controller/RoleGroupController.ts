import {DataFetcher} from "./dataFetcher/DataFetcher";
import {
    AllUsersAndRolesResponseDTO,
    SearchUserRequestDTO, UpsertRoleDTO,
    UserRoleDTO, UserRoleSearchDTO
} from "../model/dtos";
import {
    RolesSettingsState,
    UserRole,
    UserRoleCheckboxList,
    UserRoleSearch,
    UserRolesSettingsState
} from "../model/props";

export default  class RoleGroupController{
    constructor(private dataFetcher: DataFetcher) {
    }

    async showRole(): Promise<RolesSettingsState>{
        let getRoles: AllUsersAndRolesResponseDTO = await this.dataFetcher.getAllRoles();

        let dashboard: RolesSettingsState = {
            all: [],
            logged: {
                name: '',
                roles: []
            },
            roles: [],
            isLoading: false
        };

        getRoles.all.map((i)=>{
            let allUserRoles: UserRole = {
                name: i.name,
                roles: i.roles.map((j)=>{return j})
            };
            dashboard.all.push(allUserRoles)
        });
        dashboard.logged = getRoles.logged;

        return dashboard
    }

    async searchRole(name: string): Promise<any>{

        let searchUserRoleRequestDTO: SearchUserRequestDTO = {
            name: name,
        };

        let dashboard: UserRoleSearch = {
            name: '',
            editable_roles: [],
            allRoles: []
        };

        let searchUser: UserRoleSearchDTO = await this.dataFetcher.searchUser(searchUserRoleRequestDTO);
        let getRoles: AllUsersAndRolesResponseDTO = await this.dataFetcher.getAllRoles();

        getRoles.roles.map((i)=>{
            let checkRole: UserRoleCheckboxList = {
                role: i,
                checked: searchUser.editable_roles.includes(i)
            };
            dashboard.allRoles.push(checkRole)
        });

        searchUser.editable_roles.map((i)=>{
            dashboard.editable_roles.push(i)
        });
        dashboard.name=searchUser.name;

        return dashboard
    }

    async upsertRolesFromUser(name: string, rolesAdd: string[], rolesDelete: string[]): Promise<UserRolesSettingsState>{

        let upsertUserRoleRequestDTO: UpsertRoleDTO = {
            name: name,
            roles: rolesAdd
        };

        let deleteUserRoleRequestDTO: UpsertRoleDTO = {
            name: name,
            roles: rolesDelete
        };

        let dashboard: UserRolesSettingsState = {
            removeRoleArray: [], upsertRoleArray: [],
            name: name,
            isLoading: false,
            editable_roles: [],
            allRoles: []
        };

        let searchUserRoleRequestDTO: SearchUserRequestDTO = {
            name: name,
        };

        //request for insert/update and delete
        let upsertUser: any = await this.dataFetcher.upsertUserWithRoles(upsertUserRoleRequestDTO);
        let deleteRolesFromUser: any = await this.dataFetcher.deleteRolesFromUser(deleteUserRoleRequestDTO);

        let searchUser: UserRoleSearchDTO = await this.dataFetcher.searchUser(searchUserRoleRequestDTO);
        let getRoles: AllUsersAndRolesResponseDTO = await this.dataFetcher.getAllRoles();

        getRoles.roles.map((i)=>{
            let checkRole: UserRoleCheckboxList = {
                role: i,
                checked: searchUser.editable_roles.includes(i)
            };
            dashboard.allRoles.push(checkRole)
        });

        searchUser.editable_roles.map((i)=>{
            dashboard.editable_roles.push(i)
        });

        return dashboard
    }


    async insertUser(name: string, password: string): Promise<RolesSettingsState>{

        let upsertUserRoleRequestDTO: UserRoleDTO = {
            name: name,
            password: password,
            roles: []
        };

        let dashboard: RolesSettingsState = {
            all: [],
            logged: {
                name: '',
                roles: []
            }, roles: [],
            isLoading: false
        };

        let searchUserRoleRequestDTO: SearchUserRequestDTO = {
            name: name,
        };

        //request for insert/update and delete
        let upsertUser: any = await this.dataFetcher.upsertUserWithRoles(upsertUserRoleRequestDTO);

        let searchUser: UserRoleSearchDTO = await this.dataFetcher.searchUser(searchUserRoleRequestDTO);
        let getRoles: AllUsersAndRolesResponseDTO = await this.dataFetcher.getAllRoles();

        getRoles.all.map((i)=>{
            let allUserRoles: UserRole = {
                name: i.name,
                roles: i.roles.map((j)=>{return j})
            };
            dashboard.all.push(allUserRoles)
        });
        dashboard.logged = getRoles.logged;

        return dashboard
    }

}


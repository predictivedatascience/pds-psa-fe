import {DataFetcher} from "./dataFetcher/DataFetcher";
import {AuthenticationController} from "./AuthenticationController";
import {UnauthenticatedException} from "../exceptions/UnauthenticatedException";
import {change_password, add_user, del_user} from "../model/dtos";


export default class AccountController {

    constructor(private dataFetcher: DataFetcher) {

    }

    public async changePassword(username: string, newPassword: string): Promise<change_password> {

        let changePwd: change_password = await this.dataFetcher.changePassword(username, newPassword);

        if (changePwd.code !== 70) {
            throw new UnauthenticatedException(changePwd.message ? changePwd.message : 'Cant change password');
        }
        return changePwd;
    }

    public getUserName(){

        let token: string | null = AuthenticationController.getAuthenticationToken();

        let DataJWT = token.split(".")[1];
        let decodedJwtJsonData: string = window.atob(DataJWT);
        let decodedJwtData = JSON.parse(decodedJwtJsonData);
        let username = decodedJwtData.identity['name'];

        return username;
    }

    public async add_user(username: string, pwd: string, userRole:string): Promise<add_user>{
        let addUser: add_user = await this.dataFetcher.add_user(username, pwd, userRole);

        if (addUser.code !== 90) {
            throw new UnauthenticatedException(addUser.message ? addUser.message : 'Cant add user');
        }
        return addUser;
    }

    public async del_user(username: string): Promise<del_user>{
        let delUser: del_user = await this.dataFetcher.del_user(username);

        if (delUser.code !== 80) {
            throw new UnauthenticatedException(delUser.message ? delUser.message : 'Cant delete user');
        }
        return delUser;
    }
}


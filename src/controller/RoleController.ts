import {AuthenticationController} from "./AuthenticationController";

export function getUserRole(){

    let token: string | null = AuthenticationController.getAuthenticationToken();
    if(token !== null) {
        let DataJWT = token.split(".")[1];
        let decodedJwtJsonData: string = window.atob(DataJWT);
        let decodedJwtData = JSON.parse(decodedJwtJsonData);
        let roles = decodedJwtData.identity['roles'];

        return roles;
    }

}

export function isLdapUser(){

    let token: string | null = AuthenticationController.getAuthenticationToken();

    let DataJWT = token.split(".")[1];
    let decodedJwtJsonData: string = window.atob(DataJWT);
    let decodedJwtData = JSON.parse(decodedJwtJsonData);
    let ldap = decodedJwtData.identity['ldap'];

    return ldap;

}


export function getUserName(){
    let token: string | null = AuthenticationController.getAuthenticationToken();

    let DataJWT = token.split(".")[1];
    let decodedJwtJsonData: string = window.atob(DataJWT);
    let decodedJwtData = JSON.parse(decodedJwtJsonData);
    let username = decodedJwtData.identity['name'];

    return username;

}
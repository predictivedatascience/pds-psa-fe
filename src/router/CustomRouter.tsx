import React from 'react';
import { Switch } from 'react-router';
import RouteWithSubRoutes from "./RouteWithSubRoutes";
import { DataFetcher } from "../controller/dataFetcher/DataFetcher";
import { Configuration } from "../model/model";
import '@elastic/charts/dist/theme_light.css';
import routes from "./routes";

export interface IRoute {
    path: string;
    exact: boolean;
    component?: any
    routes?: IRoute[];
    private?: boolean;
    redirect?: string;
}

interface IProps {
    routes: IRoute[];
    config: Configuration;
    dataFetcher: DataFetcher;
    line: string,
    modelKeyword: string,
    date: string,
    handleDateChange: any,
    handleHalleUpdate: any,
    handleHalleChange: any,
    handleModelChange: any,

}

const CustomRouter: React.FC<IProps> = (props: any) => {

    return <Switch>
        {props.routes.map((route: JSX.IntrinsicAttributes & IRoute, i: string | number | undefined,) => {
            return <RouteWithSubRoutes
                config={props.config}
                line={props.line}
                modelKeyword={props.modelKeyword}
                date={props.date}
                dataFetcher={props.dataFetcher}
                key={i}
                handleHalleUpdate={props.handleHalleUpdate} handleDateChange={props.handleDateChange} handleModelChange={props.handleModelChange} handleHalleChange={props.handleHalleChange}
                {...route} />
        })}
    </Switch>;
};

export default CustomRouter;
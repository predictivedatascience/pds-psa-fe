import React from 'react';
import { Route } from 'react-router-dom';
import CustomRouter from "./CustomRouter";


function RouteWithSubRoutes(route: any) {
    let getRouteID = window.location.pathname.replace(/^\/([^\/]*).*$/, '$1')
    return (
        <Route
            path={route.path}
            render={({ props, match, location, history, theme }: any) => (
                <route.component {...props} {...match} {...location} {...history} {...theme} routeID={getRouteID}
                    config={route.config} dataFetcher={route.dataFetcher} routes={route.routes} line={route.line} modelKeyword={route.modelKeyword} date={route.date} handleHalleUpdate={route.handleHalleUpdate} handleDateChange={route.handleDateChange} handleModelChange={route.handleModelChange} handleHalleChange={route.handleHalleChange} />
            )}
        />
    );
}

export default RouteWithSubRoutes;
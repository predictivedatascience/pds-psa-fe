/* eslint-disable react/no-multi-comp */
/* eslint-disable react/display-name */
import React, {lazy} from 'react';

import ErrorLayout from '../layouts/Error';
import DashboardLayout from '../layouts/Dashboard';

import {PDSSpace} from "../view/components/Spaces/PDSSpace";
import {AddUserDashboard, ChangePwdDashboard, DelUserDashboard} from "../view/dashboards/AccountSettings";
import {AboutDashboard} from "../view/dashboards/AboutDashboard";
import {RolesSettings} from "../view/dashboards/RolesSettings";
import {UserRolesSettings} from "../view/dashboards/UserRolesSettings";
import {EmailSettingsDashboard} from "../view/dashboards/email/EmailSettingsDashboard";
import Error404 from "../view/errors/Error404";
import PDSSingIn from "../view/PDSSingIn";
import AuthLayout from "../layouts/Auth";


const routes = [
    {
        path: '/auth',
        component: AuthLayout,
        exact: true,
        pdfExport: false,
        private: false,
        routes: [
            {
                path: '/auth/login',
                exact: true,
                pdfExport: false,
                component: PDSSingIn
            },
            {
                path: '*',
                exact: true,
                pdfExport: false,
                redirect: '/errors/error-404',
                component: Error404
            }
        ]
    },
    {
        path: '/errors',
        component: ErrorLayout,
        private: false,
        pdfExport: false,
        exact: true,
        routes: [
            {
                path: '/errors/error-401',
                exact: true,
                pdfExport: false,
                component: lazy(() => import('../view/errors/Error401'))
            },
            {
                path: '/errors/error-403',
                exact: true,
                pdfExport: false,
                component: lazy(() => import('../view/errors/Error403'))
            },
            {
                path: '/errors/error-404',
                exact: true,
                pdfExport: false,
                component: lazy(() => import('../view/errors/Error404'))
            },
            {
                path: '/errors/error-500',
                exact: true,
                pdfExport: false,
                component: lazy(() => import('../view/errors/Error404'))
            },
            /*{
                component: () => <Redirect to="/errors/error-404" />
            }*/
        ]
    },
    {
        path: '*',
        component: DashboardLayout,
        private: true,
        exact: true,
        pdfExport: false,
        routes: [
            {
                path: '/',
                exact: true,
                component: (props: any) => <PDSSpace {...props} />
            },
            {
                path: '/spaces',
                exact: true,
                component: (props: any) => <PDSSpace {...props} />
            },
            {
                path: '/account_settings',
                exact: true,
                component: ChangePwdDashboard
            },
            {
                path: '/roles_settings',
                exact: true,
                pdfExport: false,
                component: RolesSettings
            },
            {
                path: '/roles_settings/user/:name/:action',
                exact: true,
                pdfExport: false,
                component: UserRolesSettings

            },
            {
                path: '/add_user',
                exact: true,
                pdfExport: false,
                component: AddUserDashboard
            },
            {
                path: '/delete_user',
                exact: true,
                pdfExport: false,
                component: DelUserDashboard
            },
            {
                path: '/about',
                exact: true,
                pdfExport: false,
                component: AboutDashboard
            },
            {
                path: '/email',
                exact: true,
                pdfExport: true,
                component: EmailSettingsDashboard
            },
            {
                path: '*',
                exact: true,
                pdfExport: false,
                redirect: '/errors/error-404',
                component: Error404
            }
        ]
    }
];

export default routes;

import {
    Pair,
} from "./model";

/** Interface defining props UserRole consisting of two properties */
export interface UserRole {
    /** This property represents name of user. */
    name: string,
    /** This property represents roles of the user. */
    roles: string[],
}

/** Interface defining props UserRoleSearch consisting of 3 properties. */
export interface UserRoleSearch {
    /** This property represents name of user. */
    name: string,
    /** This property represents editable roles displayed that could be asigned. */
    editable_roles: string[],
    /** This property represents list of roles to be displayed in the checkbox select container. */
    allRoles: UserRoleCheckboxList[];
}

/** Interface defining props UserRoleCheckboxList consisting of 2 properties. */
export interface UserRoleCheckboxList {
    /**This property represents role of the user.*/
    role: string;
    /** This property represents chceckbox status, is true if chcecked.*/
    checked: boolean;
}

/** Interface defining app-state of UserRolesSettings dashboard consisting of 5 properties. */
export interface RolesSettingsState {
    /** This property represents currently logged user. */
    logged: UserRole;
    /** This property represents list of all available roles for the user. */
    all: UserRole[];
    /** This property represents list of all selected roles of the user. */
    roles: string[];
    /** This property represents whether the page is still loading or has already been loaded. */
    isLoading: boolean;
    errorMsg?: string;
}

/** Interface defining app-state of UserRolesSettings dashboad consisting of 8 properties */
export interface UserRolesSettingsState {
    /** This property represents name of user. */
    name: string;
    /** This property represents list of all available roles for the user. */
    allRoles: UserRoleCheckboxList[];
    /** This property represents editable roles displayed that could be assigned. */
    editable_roles: string[];
    /** This property represents whether the page is still loading or has already been loaded. */
    isLoading: boolean;
    errorMsg?: string;
    action?: string;
    removeRoleArray: string[],
    upsertRoleArray: string[],
}

/** nema vyskyt */
export interface UserRoleState {
    /** This property represents name of user. */
    name: string;
    /** This property represents roles of the user. */
    roles: string[];
    /** This property represents whether the page is still loading or has already been loaded. */
    isLoading: boolean;
    errorMsg?: string;
}


/************** Email ************/
export interface EmailTableProps {
    dataFetcher: any;
    data: Email[];
    sort: string;
    saveFunction: any;
    deleteFunction: any;
    sortFunction: any;
    role:any;
}

export interface EmailsDashboardState {
    spaces: Email[];
    sort: string;
    isLoading: boolean;
    errorMsg?: string;
    deleteProps?: ConfirmDialogProps;
}

export interface Email {
    [key: string]: any;
    id?: string,
    space:string,
    subject: string,
    body: string,
    recipients: EmailRecipient[],
    attachment: EmailAttachment,
    sender?: string
}



export interface EmailRecipient {
    id?: string;
    name: string;
    mail: string;
}

export interface EmailAttachment {
    file_path: string;
    file_name: string;
    send_as: string;
    extension: string;
}
export interface ConfirmDialogProps {
    title: string;
    message: string;
    onConfirm: any;
    onClose: any;
    open: boolean;
    options?: Pair<string>;
}





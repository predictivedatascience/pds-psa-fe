export interface Localization{
    common:{
        user:string
        space:string
        dashboard_cannot_be_loaded: string
        add:string
        cancel:string
        language:string
        edit:string
        remove:string
        save:string
        model:string
        all:string
        search:string
        last_update:string
        export_csv:string
        my_dashboard: string
        account_settings: string
        email_settings: string,
        about: string,
        log_out: string,
        add_user: string,
        confirm: string,
        create_new_dashboard: string,
        remove_dashboard:string,
        unauthorized_message:string,
        select_space: string,
        rows: string,
        anzahl:string,
        time_from:string,
        time_to:string,
        select_model:string
    }
    models:{
        all:string
    }
    space:{
        select_your_space:string
        no_spaces_available:string
    }
    user:{
        username:string;
        password:string;
        log_out:string;
        log_in:string;
        delete:string;
        delete_success:string;
        add:string;
        role:string;
        new_password:string;
        new_password_confirm:string;
        add_success:string;
        passwords_not_match:string;
        change_password:string;
        change_password_success:string;
        id:string;
        role_settings:string;
        users:string;
        current_user:string;
        roles:string;
        actual_roles:string;
        edit:string;
        edit_roles:string;
        add_role:string;
        save_roles:string;
        current_roles:string;
        user_roles:string;
        name: string,
        actions: string,
        add_charts:string,
        setup:string
    }
    materialTable: materialTableLocalization|undefined
    uvd:{
        umlaufverweildauer:string
        duration:string
        total:string
    }
    email: {
        email_einstellungen: string
        nachrichtentext: string
        empfanger: string
        absendername: string
        anhange: string
        addresse: string
        dateiname: string
        speicherort: string
        dateiendung: string
        verschicken_als :string
        row_cannot_be_saved: string,
        delete_space: string,
        delete_confirmation: string,
        row_cannot_be_delete:string
    }
    schraubdaten: {
        schraubdaten:string,
        ubersicht:string,
        schraubdaten_ubersicht: string,
        schraubstellen: string,
        kotfluge: string,
        seitentei: string,
        passung_fehlerhaf: string,
        gewinde_beschadigt: string,
        betreff: string,
        date:string,
        fehlern:string
    }
    sap_fi:{
        select_date_from_to: string,
        from:string,
        to: string,
        close: string,
        agree: string,
        save_all_charts:string,
        edit_title: string,
        settings:string,
        exit_settings:string,
        remove_chart:string,
        create_chart: string,
        chart_settings: string,
        data:string,
        calendar: string,
        change_grid: string,
        select_chart: string,
        chart_width: string,
        add_new_data: string,
        select_first_chart:string,
        data_source:string,
        fehleranzahl:string,
        DLQ_monthly: string,
        kosten:string,
        line:string,
        bar: string,
        scatter: string,
        red: string,
        yellow:string,
        orange: string,
        light_green:string,
        teel: string,
        cyan: string,
        light_blue: string,
        indigo: string,
        purple: string,
        material:string
        produktionsschrott: string,
        investition: string,
        instandhaltung: string,
        verschleisswerkzeug: string,
        energie: string,
        sonstige: string,
        nacharbeit: string,
        beschaffungsnebenkosten: string,
        dienstleistungen: string,
        reisekosten: string,
        miete: string,
        weiterbildung:string,
        IT: string,
        personal: string,
        select_y_data: string,
        select_checkpoint: string,
        select_line_type: string,
        select_color: string
    },
    direktlauf: {
        karosserie_bau: string,
        montage_perceptron: string,
        montage_bandende: string,
        direktlauf_passungen: string,
        entwicklung_schicht: string,
        entwicklung_tag: string,
        entwicklung_woche: string,
        entwicklung_monate: string,
        dlq_gesamt: string,
        dlq_frontklappe: string,
        dlq_tur: string,
        dlq_heck: string,
        ohne_tür: string,
        details: string
    },
    wsus: {
        hostnames_by_patches: string,
        patches_by_hostnames: string,
        wsus_number_patches_by_patchname_kb: string,
        wsus_number_of_patches_by_hosts: string,
        not_installed: string,
        downloaded: string,
        failed: string,
        installed: string,
        systemname: string,
        patch_name: string,
        kb_article_rev: string,
        classification: string,
        wsus_gruppe: string,
        instalation_state: string,
        database: string,
        loading_data: string,
        exporting: string,
        export_data_by: string,
        group_membership: string,
        export: string,
        loading_options:string,
        first_page:string,
        previous_page:string,
        next_page:string,
        last_page:string,
        loading_table: string
        cannot_load: string,
        export_table_to_excel: string,
        error_loading_table: string
    }
    atlas: {
        atlas_dashboard: string
        summary_table: string,
        anlaufbericht: string,
        atlas_umlauftabelle: string,
        previous: string,
        next: string,
        reinbetriebnahme: string,
        atlas_table:string,
        stellplatz:string
    },
    audit: {
        karoaudit: string,
        korroaudit: string,
        lackaudit: string,
        montaudit: string,
        gesamtbericht_karosseriebau: string,
        entwicklung_tag: string,
        entwicklung_woche: string,
        entwicklung_monat: string,
        beanstandung: string,
        bauteil: string,
        beanstandungen_tag: string,
        beanstandungen_woche: string,
        beanstandungen_monat: string,
        unknown_dashboard_type:string,
        unknown_2lvl_dashboard_type:string,
        gesamtbericht:string,
        not_found: string
    },
    produktion: {
        umlauftabelle :string,
        direktlauf_ab_bandende_montagen_zwickau :string,
        halle :string,
        gesamt_tagesbericht :string,
        berichtstag :string,
        stand :string,
        punkt :string,
        puffer_vor_af :string,
        ausschlesung :string,
        anzahl_durchläufe :string,
        umlauf :string,
        keine_hvibn :string,
        keine_befüllung :string,
        fehlerobjekt :string,
        fehlerlage: string
        fehlerart :string,
        fehler_gesamt :string,
        fehler_je_fahrzeug :string,
        rp_geräusche :string,
        rolle :string,
        funktion :string,
        top10 :string,
        dichtigkeitsprüfanlage :string,
        scheinwerfereinstellstand :string,
        oberfläche :string,
        fehlertabelle: string,
        pin:string,
        prod_jahr:string,
        zeitstempelBeheb:string,
        zeitstempelErfass:string,
        behebungsort:string,
        fehlart: string
    },
    dlq_be: {
        select_model_and_halle: string,
        befullung: string,
        verschraubungen: string,
        rest: string
    },
    flashaktion: {
        anlaufbericht: string,
        ohne_sonstige_fahrzeuge:string,
        reinbetriebnahme:string,
        anzahl_M800:string,
        anzahl_ZP8:string,
        unauthorized_access:string,
        stellplatz:string,
        akt_umlauf_grunes:string,
        akt_umlauf_Z899: string,
        davon_bereits_versendet:string,
        davon_aktuell_versandbereit:string,
        davon_aktuell_nicht_versandbereit: string,
        fehlende_feldschlussel: string,
        RL_sperre: string
    },
    gesamtfehler: {
        gesamtfehler_dashboard: string,
        gesamtfehler: string,
        bohebungsort_beschreibung: string,
        zeitstempel_fehlerabgearbaitet: string,
        status_aktuell: string
    },
    ofiu: {
        offene_fehler_im_umlauf: string,
        ohne_passungsfehler: string,
        fehlerobjekt_beschreibung: string,
        fehlerart_beschreibung: string,
        verursacher: string,
        zeitstempel_eintragungzeit: string,
        plr_checkpoin_aktuellstatus: string
    },
    mqtt: {
        prepressure: string,
        temperature: string,
        volume: string,
        aplication_time: string,
        flow: string,
        pressure: string,
        torque: string,
        lastfilltime: string,
        lastfillvolume: string,
        realvalues: string,
        cycletype: string,
        result: string,
        monitoring_MQTT:string
    },
    opcua: {
        machines:string,
        table_view:string,
        graphic_view:string,
        display_wfs:string,
        display_current:string,
        displayvoltage:string,
        display_energy:string,
        display_power:string,
        all_displays:string,
        wfs_command_value:string,
        voltage_recomm_value:string,
        gas_value:string,
        wfs_recomm_display:string,
        current_recomm_display:string,
        voltage_recomm_display:string,
        motor_force_m1:string,
        motor_force_m2:string,
        all_motor_force:string,
        cooler_flow:string,
        cooler_temp:string,
        job_number:string,
        error_bar:string,
        error_pie:string,
        errors_per_12_hours:string,
        monitoring_OPCUA:string
    },
    rtp: {
        road_test_prediction: string
    },
    schleppeliste: {
        schleppeliste_dashboard: string,
        schleppeliste: string,
        anmerkung: string,
        anderungsdatum: string,
        actions: string
    }
    
    
}


export interface LanguageContexValue{
    language:"EN"|"DE",
    translations:Localization
    handleLanguageChange: (language: "EN"|"DE")=>void
}

interface materialTableLocalization{
    body: {
        emptyDataSourceMessage: string,
        addTooltip: string,
        deleteTooltip: string,
        editTooltip: string,
        filterRow: {
            filterTooltip: string,
        },
        editRow: {
            deleteText: string,
            cancelTooltip: string,
            saveTooltip: string,
        }
    },
    grouping: {
        placeholder: string,
        groupedBy: string,
    },
    header: {
        actions: string,
    },
    pagination: {
        labelDisplayedRows: string,
        labelRowsSelect: string,
        labelRowsPerPage: string,
        firstAriaLabel:  string,
        firstTooltip:  string,
        previousAriaLabel:  string,
        previousTooltip:  string,
        nextAriaLabel:  string,
        nextTooltip:  string,
        lastAriaLabel:  string,
        lastTooltip:  string,
    },
    toolbar: {
        addRemoveColumns:  string,
        nRowsSelected:  string,
        showColumnsTitle:  string,
        showColumnsAriaLabel:  string,
        exportTitle: string,
        exportAriaLabel: string,
        exportName: string,
        searchTooltip: string,
        searchPlaceholder:  string
    },
    
   
}
export interface change_password {
    code: number; // represents HTTP response code
    message?: string;
}

export interface add_user {
    code: number; // represents HTTP response code
    message?: string;
}

export interface del_user {
    code: number; // represents HTTP response code
    message?: string;
}

export interface SearchUserRequestDTO {
    name: string;
}

export interface AllUsersAndRolesResponseDTO {
    all: UserRoleDTO[];
    logged: UserRoleDTO;
    roles:string[];
}

export interface UserRoleDTO {
    name: string;
    password: string;
    roles: string[];
}

export interface UpsertRoleDTO {
    name: string;
    roles: string[];
}

export interface UserRoleSearchDTO {
    name: string;
    roles: string[];
    /** This property represents editable displayed roles that could be asigned. */
    editable_roles: string[];
}

export interface AuthResult {
    code: number; // represents HTTP response code
    message?: string;
    token?: string;
    ldap?: string;
}

export interface RoleResult {
    code: number; // represents HTTP response code
    message?: string;
    token?: string;
}

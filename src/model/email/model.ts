import {Email} from "../props";

export interface EmailInterface {
    getEmailData():Promise<Email[][]>
    saveEmailData(request:Email):Promise<any>
    addEmailSpace(dataRequest: Email): Promise<any>
    deleteEmailSpace(dataRequest: {space:string}): Promise<any>
    updateEmailSkipDates(dataRequest: {space_id:number, skip_dates:string[]}):Promise<any>
    getEmailSkipDates(dataRequest: {space_id:number}):Promise<any>
}
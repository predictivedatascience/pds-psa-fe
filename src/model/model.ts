import {
    SearchUserRequestDTO,
    UserRoleDTO,
    AllUsersAndRolesResponseDTO,
} from "./dtos";

export interface IDataProvider {
    //ROLES
    getAllRoles(): Promise<AllUsersAndRolesResponseDTO>;
    getCurrentRole(): Promise<any>;
    searchUser(dataRequest: SearchUserRequestDTO): Promise<any>;
    upsertUserWithRoles(dataRequest: UserRoleDTO): Promise<any>;
    deleteRolesFromUser(dataRequest: UserRoleDTO): Promise<any>;
}

export interface ITableRow {
    columns: string[];
}

export interface ITable {
    header?: ITableRow;
    rows: ITableRow[];
}

export interface ChartValues {
    x: any;
    y: number;
}

export interface auditChartValuesA {
    xA1: any;
    yA1: number;
}

export interface auditChartValuesB {
    xB1: any;
    yB1: number;
}

export interface auditChartValuesC {
    xC1: any;
    yC1: number;
}

export interface zielValues {
    date: string;
    value: number;
}

export interface LinearValues {
    x: any;
    y: number;
    x1: any;
}
export interface DataFieldsGroupedByCount {
    fields: string[];
    count: number;
}

export interface PieChartValue {
    label: string;
    value: number;
}

export interface ManufacturingError {
    modelKeyword: string;
    zpKeyword: string;
    errorType: string;
    errorObject: string;
    errorLocation: string;
    timestamp: Date;
    count: number;
}

export interface Pair<T> {
    first: T;
    second: T;
}

export interface Configuration {
    backendURL: string;
}

export interface Limits {
    max?:any;
    min?:any;
}

export interface SpaceGridStructure {
    hidden: any,
    href: string,
    title: string,
    icon:  string | JSX.Element
}

export interface SpaceMenuItemStructure {
    hidden?: boolean,
    href: string,
    title: string,
    icon: string
}

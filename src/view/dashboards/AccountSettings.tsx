import React from "react";
import { Grid, Typography } from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { DataFetcher } from "../../controller/dataFetcher/DataFetcher";
import AccountController from "../../controller/AccountController";
import { DrawerList } from "../../layouts/Dashboard/components/NavBar/components/DrawerList";
import { getUserRole } from "../../controller/RoleController";
import { LanguageContext } from "../../context/LanguageContext";
import { Localization } from "../../model/localization";


interface AccountSettingsState {
    waitingForPasswordChange?: boolean;
    errorMsg?: string;
    errorMsgShown?: boolean;
}

interface AccountSettings {
    dataFetcher: DataFetcher;
}

export class DelUserDashboard extends React.Component<AccountSettings, AccountSettingsState> {

    private paperStyles: any;
    private accountController: AccountController;

    constructor(props: any) {
        super(props);

        this.accountController = new AccountController(this.props.dataFetcher);
        this.initStyles();

        this.handleSubmittedForm = this.handleSubmittedForm.bind(this);
        this.delUser = this.delUser.bind(this);

    }

    private initStyles() {
        this.paperStyles = {
            width: "100%",
            height: "100%",
            padding: "2% 10px 10px 10px",
        };
    }

    private handleSubmittedForm(event: any) {
        event.preventDefault();

        let object: any = {};
        const data: FormData = new FormData(event.target);

        data.forEach((value, key) => {
            object[key] = value
        });

        this.delUser(object.Username);
        window.alert(`${this.context.translations.user.delete_success}`);
    }

    private async delUser(username: string) {

        this.setState({ waitingForPasswordChange: true });

        this.accountController.del_user(username).then(
            (onFulfilled) => {
                this.setState({ waitingForPasswordChange: false });
            },
            (onRejected) => {
                //console.log('onRejected = ' + onRejected);
                this.setState({
                    waitingForPasswordChange: false,
                    errorMsgShown: true,
                    errorMsg: onRejected.toString()
                });
            });
    }
    static contextType = LanguageContext
    render() {
        const translations: Localization = this.context.translations;
        return (
            <div style={{ marginTop: '50px' }}>
                <Grid item xs>
                    <Typography variant="h5" component="h3" style={{ textAlign: 'center' }}>
                        {translations.user.delete}
                    </Typography>
                </Grid>
                <form style={{ padding: "auto", margin: "auto", width: "50%" }} onSubmit={this.handleSubmittedForm}>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        name="Username"
                        label={translations.user.username}
                        id="Username"
                    />
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                    >
                        {translations.user.delete}
                    </Button>
                </form>
            </div>
        )
    }
}

export class AddUserDashboard extends React.Component<AccountSettings, AccountSettingsState> {

    private paperStyles: any;
    private accountController: AccountController;
    private DrawerList: DrawerList;

    constructor(props: any) {
        super(props);

        this.DrawerList = new DrawerList(this);
        this.accountController = new AccountController(this.props.dataFetcher);
        this.initStyles();

        this.handleSubmittedForm = this.handleSubmittedForm.bind(this);
        this.addUser = this.addUser.bind(this);
    }

    private initStyles() {
        this.paperStyles = {
            width: "100%",
            height: "100%",
            padding: "2% 10px 10px 10px",
        };
    }

    private handleSubmittedForm(event: any) {
        event.preventDefault();

        let object: any = {};

        const data: FormData = new FormData(event.target);

        data.forEach((value, key) => {
            object[key] = value
        });

        if (object.newPassword === object.newPasswordConfirm) {
            // @ts-ignore
            var UserRole = document.getElementById("UserRole").value;
            this.addUser(object.newUsername, object.newPassword, UserRole);
            window.alert(`${this.context.translations.user.add_success}`);
        }
        else {
            window.alert(`${this.context.translations.user.passwords_not_match}`);
        }
    }

    private async addUser(username: string, newPassword: string, UserRole: string) {

        this.setState({ waitingForPasswordChange: true });

        this.accountController.add_user(username, newPassword, UserRole).then(
            (onFulfilled) => {
                this.setState({ waitingForPasswordChange: false });
            },
            (onRejected) => {
                //console.log('onRejected = ' + onRejected);
                this.setState({
                    waitingForPasswordChange: false,
                    errorMsgShown: true,
                    errorMsg: onRejected.toString()
                });
            });
    }

    static contextType = LanguageContext
    render() {
        const translations: Localization = this.context.translations;
        return (
            <div style={{ marginTop: '50px' }}>
                <Grid item xs>
                    <Typography variant="h5" component="h3" style={{ textAlign: 'center' }}>
                        {translations.user.add}
                    </Typography>
                </Grid>
                <form style={{ padding: "auto", margin: "auto", width: "50%" }} onSubmit={this.handleSubmittedForm}>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        name="newUsername"
                        label={translations.user.username}
                        id="newUsername"
                    />
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        name="newPassword"
                        label={translations.user.new_password}
                        type="password"
                        id="newPassword"
                    />
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        name="newPasswordConfirm"
                        label={translations.user.new_password_confirm}
                        type="password"
                        id="newPasswordConfirm"
                    />
                    <label htmlFor="UserRole" style={{ margin: '10px' }}>{translations.user.user_roles}: </label>
                    <select id="UserRole">
                        <option value="ADMIN">Admin</option>
                        <option value="ATLAS">Atlas</option>
                        <option value="QSAUDIT">QSAudit</option>
                        <option value="QSMR">QSMR</option>
                        <option value="QSPR">QSPR</option>
                        <option value="WSUS">WSUS</option>
                        <option value="EMAIL_CONFIG">EMAIL_CONFIG</option>
                    </select>

                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        style={{ marginTop: "10px" }}
                    >
                        {translations.user.add}
                    </Button>
                </form>
            </div>
        )
    }
}

export class ChangePwdDashboard extends React.Component<AccountSettings, AccountSettingsState> {

    private paperStyles: any;
    private accountController: AccountController;
    role: string;

    constructor(props: any) {
        super(props);

        this.accountController = new AccountController(this.props.dataFetcher);
        this.initStyles();

        this.handleSubmittedForm = this.handleSubmittedForm.bind(this);
        this.changePassword = this.changePassword.bind(this);

        this.role = getUserRole();
    }

    private initStyles() {
        this.paperStyles = {
            width: "100%",
            height: "100%",
            padding: "2% 10px 10px 10px",
        };
    }

    private handleSubmittedForm(event: any) {
        event.preventDefault();

        let object: any = {};
        let username: string = this.accountController.getUserName();

        const data: FormData = new FormData(event.target);

        data.forEach((value, key) => {
            object[key] = value
        });

        if (object.newPassword === object.newPasswordConfirm) {
            this.changePassword(username, object.newPassword);
            window.alert(`${this.context.translations.user.change_password_success}`);
        }
        else {
            window.alert(`${this.context.translations.user.passwords_not_match}`);
        }
    }

    private async changePassword(username: string, newPassword: string) {

        this.setState({ waitingForPasswordChange: true });

        this.accountController.changePassword(username, newPassword).then(
            (onFulfilled) => {
                this.setState({ waitingForPasswordChange: false });
            },
            (onRejected) => {
                //console.log('onRejected = ' + onRejected);
                this.setState({
                    waitingForPasswordChange: false,
                    errorMsgShown: true,
                    errorMsg: onRejected.toString()
                });
            });
    }
    static contextType = LanguageContext

    render() {
        const translations: Localization = this.context.translations;
        console.log(this.props)
        return (
            <div style={{ marginTop: '50px' }}>
                <Grid item xs>
                    <Typography variant="h5" component="h3" style={{ textAlign: 'center' }}>
                        {translations.user.change_password}
                    </Typography>
                </Grid>
                <form style={{ padding: "auto", margin: "auto", width: "50%" }} onSubmit={this.handleSubmittedForm}>
                    <TextField style={{ justifyItems: "center" }} variant="outlined" margin="normal" required fullWidth name="newPassword" label={translations.user.new_password} type="password" id="newPassword"
                    />
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        name="newPasswordConfirm"
                        label={translations.user.new_password_confirm}
                        type="password"
                        id="newPasswordConfirm"
                    />
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                    >
                        {translations.user.change_password}
                    </Button>
                </form>
            </div>
        )
    }
}
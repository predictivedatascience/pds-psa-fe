import {Grid, Typography,} from "@material-ui/core";
import * as React from "react";
import 'typeface-roboto';
import '../../custom.css'


export function AboutDashboard(props:any) {

    return (
        <React.Fragment>
            <div style={{paddingTop:"40px"}}>
                <Grid
                    container
                    direction="row"
                    justify="center"
                    alignItems="center"
                    xs={10}
                >
                <table style={{width:'50%', border: "0.1em solid darkgrey"}}>
                    <tr>
                        <th style={{backgroundColor:"#F5F7FA", border: "0.1em solid darkgrey"}}>
                            <Typography variant="h4" component="h5" align="center">
                                About
                            </Typography>
                        </th>
                    </tr>
                    <Grid
                        container
                        direction="row"
                        justify="center"
                        alignItems="center"
                    >
                        <img src="/img/pds_logo_short.png" alt="VW logo" height="70px"/>
                    </Grid>
                    <Typography variant="h4" component="h5" align="center">
                        PDS IDA
                    </Typography>
                    <Typography align="center" style={{fontSize:"20px"}}>
                        <div>Based on PDS Artificial Intelligence Platform</div>
                        <div style={{marginBottom:"15px"}}>v1.11.1 -  build 20210301</div>
                        <div>Copyright ©2021 PDS - PredictiveDataScience. All rights reserved.</div>
                        <div style={{marginBottom:"15px"}}>Web: <a href={"https://www.predictivedatascience.sk/de"}>predictivedatascience.sk/de</a> E-mail: info@predictivedatascience.sk</div>

                    </Typography>
                </table>

                    <Typography style={{fontSize:"15px", position: "absolute", bottom: "10px", textAlign: "center", width:"99.7%"}}>
                        <img src="/img/pds_logo.jpg" alt="PDS logo" height="40px" width="300px"style={{marginBottom:'15px'}}/>
                        <div>PredictiveDataScience s.r.o., Klzava, 31 831 01 Bratislava is the copyright owner. The content of the VW AIP can only be used for the purposes</div>
                        <div>of the defined contractual relationship between Volkswagen Sachsen GmbH and PredictiveDataScience s.r.o. Any other use or distribution of the</div>
                        <div>VW AIP, whether in whole or in parts, is prohibited without the prior written consent of PredictiveDataScience s.r.o.</div>
                    </Typography>
                </Grid>

            </div>
        </React.Fragment>
    )
}
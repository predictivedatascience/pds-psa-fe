import React, { Component } from 'react';
import { UserRoleCheckboxList, UserRolesSettingsState, UserRoleState } from "../../model/props";
import RoleGroupController from "../../controller/RoleGroupController";
import { Card, Grid, InputAdornment, Paper, TextField, Typography } from "@material-ui/core";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import CardContent from "@material-ui/core/CardContent";
import Button from '@material-ui/core/Button';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import CardActions from '@material-ui/core/CardActions';
import AddIcon from '@material-ui/icons/Add';
import IconButton from '@material-ui/core/IconButton';
import { getUserRole } from "../../controller/RoleController";
import { Link as RouterLink } from "react-router-dom";
import { LanguageContext } from "../../context/LanguageContext";
import { Localization } from "../../model/localization"



export interface dashboardState extends UserRolesSettingsState {
    title: string,
    openDialog: boolean,
    newInputRole: UserRoleCheckboxList,
}

export class UserRolesSettings extends Component<any, dashboardState>{

    private roleGroupController: RoleGroupController;
    role: string;

    constructor(props: any) {
        super(props);
        this.handleEditChange = this.handleEditChange.bind(this);
        this.handleEditClick = this.handleEditClick.bind(this);
        this.handleInputClick = this.handleInputClick.bind(this);
        this.handleInputSubmit = this.handleInputSubmit.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);

        this.state = {
            name: this.props.match.params.name,
            action: this.props.match.params.action,
            isLoading: true,
            title: '',
            allRoles: [],
            removeRoleArray: [],
            upsertRoleArray: [],
            editable_roles: [],
            openDialog: false,
            newInputRole: {
                role: '',
                checked: false
            },
        };
        this.role = getUserRole();
        this.roleGroupController = new RoleGroupController(this.props.dataFetcher);
    }

    async componentDidMount() {
        try {
            let appState: UserRolesSettingsState = await this.roleGroupController.searchRole(this.state.name);
            this.setState(appState);
        } catch (e) {
            this.setState({ allRoles: [], removeRoleArray: [], upsertRoleArray: [], isLoading: false, errorMsg: `${this.context.translations.common.dashboard_cannot_be_loaded}: ${e}!` });
        }
        if (this.state.action === 'edit') {
            this.setState({ title: 'Edit Roles' });
        }
    }

    private handleEditClick = async () => {
        try {
            let appState: UserRolesSettingsState = await this.roleGroupController.upsertRolesFromUser(this.state.name, this.state.upsertRoleArray, this.state.removeRoleArray);
            this.setState(appState);
            this.setState({ openDialog: false });
            //console.log(appState)
        } catch (e) {
            this.setState({ allRoles: [], removeRoleArray: [], upsertRoleArray: [], isLoading: false, errorMsg: `${this.context.translations.common.dashboard_cannot_be_loaded}: ${e}!` });
        }
    };

    private handleEditChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        event.persist();
        let selectedRole = this.state.allRoles.filter((name) => name.role === event.target.name);
        selectedRole[0].checked = event.target.checked;
        if (event.target.checked) {
            this.setState({ removeRoleArray: this.state.removeRoleArray.filter((name) => name !== event.target.name) });
            if (!this.state.editable_roles.includes(event.target.name)) {
                this.setState((state) => { return { upsertRoleArray: [...this.state.upsertRoleArray, event.target.name] } });
            }
        } else {
            this.setState({ upsertRoleArray: this.state.upsertRoleArray.filter((name) => name !== event.target.name) });
            if (this.state.editable_roles.includes(event.target.name)) {
                this.setState((state) => { return { removeRoleArray: [...this.state.removeRoleArray, event.target.name] } });
            }
        }
    };

    handleClickOpen = () => {
        this.setState({ openDialog: true })
    };

    handleClose = () => {
        this.setState({ openDialog: false })
    };

    handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const newRole = event.target.value;
        this.setState((state) => { return { newInputRole: { role: newRole, checked: false } } })
    };

    private handleInputSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        if (this.state.newInputRole) {
            this.setState((state) => { return { allRoles: [...this.state.allRoles, this.state.newInputRole] } })
        }
    };

    private handleInputClick = () => {
        if (this.state.newInputRole) {
            this.setState((state) => { return { allRoles: [...this.state.allRoles, this.state.newInputRole] } })
        }
    };


    static contextType = LanguageContext
    render() {
        const translations: Localization = this.context.translations;

        if (this.role.includes("MANAGE") || this.role.includes("ADMIN")) {
            console.log(this.props)
            const editFormGroup = (
                this.state.allRoles.map((i) => {
                    return (
                        <FormControlLabel
                            control={<Checkbox color="primary" onChange={this.handleEditChange} name={i.role || ''} checked={i.checked || false} />}
                            label={i.role || ''}
                        />
                    )
                })
            );

            const addRoleInput = (
                <form noValidate autoComplete="off" onSubmit={this.handleInputSubmit} >
                    <TextField
                        id="standard-basic"
                        label={`${translations.user.add_role}`}
                        style={{ width: '100%' }}
                        onChange={this.handleInputChange}
                        InputProps={{
                            endAdornment: (
                                <InputAdornment position={'end'}>
                                    <IconButton edge="end" aria-label="Add New Role" onClick={this.handleInputClick}>
                                        <AddIcon />
                                    </IconButton>
                                </InputAdornment>
                            )
                        }} />
                </form>
            );

            const editBtnForm = (
                <div>
                    <Button
                        variant="contained"
                        color={"primary"}
                        startIcon={<CloudUploadIcon />}
                        onClick={this.handleClickOpen}
                    >
                        {translations.user.edit}
                    </Button>

                    <Dialog
                        open={this.state.openDialog}
                        onClose={this.handleClose}
                        aria-labelledby="alert-dialog-title"
                        aria-describedby="alert-dialog-description"
                        maxWidth={'xs'}
                        fullWidth={true}
                    >
                        <DialogTitle id="alert-dialog-title">{`${translations.user.save_roles}?`}</DialogTitle>
                        <DialogContent>
                            {this.state.upsertRoleArray.length !== 0 &&
                                <DialogContentText id="alert-dialog-description">
                                    {translations.common.add}: {this.state.upsertRoleArray.map((i) => {
                                        return `${i} `
                                    })}
                                </DialogContentText>
                            }
                            {this.state.removeRoleArray.length !== 0 &&
                                <DialogContentText id="alert-dialog-description">
                                    {translations.common.remove}: {this.state.removeRoleArray.map((i) => {
                                        return `${i} `
                                    })}
                                </DialogContentText>
                            }
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={this.handleClose} color="inherit">
                                {translations.common.cancel}
                            </Button>
                            {this.state.removeRoleArray.length === 0 && this.state.upsertRoleArray.length === 0 ?
                                <Button variant="contained" disabled> Save </Button>
                                :
                                <Button onClick={this.handleEditClick} variant="contained" style={{ 'backgroundColor': '#009624', 'color': '#eeeeee' }} autoFocus> {translations.common.save}</Button>
                            }

                        </DialogActions>
                    </Dialog>
                </div>
            );

            const roleManagement = (
                <Button
                    variant="contained"
                    color="inherit"
                    startIcon={<ArrowBackIcon />}
                    component={RouterLink}
                    to="/roles_settings"
                >
                    {translations.user.role_settings}
                </Button>
            );

            let showRemoveArray = (
                <Typography variant="body2" component="p" style={{ 'color': '#b71c1c' }}>
                    {translations.common.remove}: {this.state.removeRoleArray.map((i) => {
                        return `${i} `
                    })}
                </Typography>
            );

            let showEditArray = (
                <Typography variant="body2" component="p" style={{ 'color': '#009624' }}>
                    {translations.user.add_role}: {this.state.upsertRoleArray.map((i) => {
                        return `${i} `
                    })}
                </Typography>
            );

            let showActualRolesArray = (
                <Typography variant="body2" component="p" style={{ 'color': '#0026ca' }}>
                    {translations.user.actual_roles}: {this.state.editable_roles.map((i) => {
                        return `${i} `
                    })}
                </Typography>
            );

            let showFormGroup;
            let showBtnGroup;
            let showRoleActual;
            if (this.state.action === 'edit') {
                showFormGroup = editFormGroup;
                showBtnGroup = editBtnForm;
                showRoleActual = showActualRolesArray
            }

            return (
                <Grid container
                    direction="row"
                    justify="center"
                    alignItems="center">
                    <Grid item xs={12}>
                        <Paper variant="outlined" className={"dashboardHeader"} style={{ margin: "5px 3px 25px 3px" }}>
                            <Grid container justify="center">
                                <Grid item xs={10}>
                                    <Typography variant="h5" component="h3" style={{ textAlign: 'center', marginLeft: "5px" }}>
                                        {this.state.title}
                                    </Typography>
                                </Grid>
                            </Grid>
                        </Paper>
                    </Grid>
                    <Grid item xs={12} lg={10}>
                        <Grid container
                            direction="row"
                            justify="center"
                            alignItems="flex-start"
                        >
                            <Grid item xs={5} lg={4} xl={4}>
                                <Card variant="outlined" style={{ 'height': '180px', 'padding': '5px', 'margin': '2%' }}>
                                    <CardContent>
                                        <Typography color="textSecondary" gutterBottom>
                                            {translations.user.users}
                                        </Typography>
                                        <Typography variant="h4" component="h2">
                                            {this.state.name}
                                        </Typography>
                                        {showRoleActual}
                                        {this.state.upsertRoleArray.length !== 0 && showEditArray}
                                        {this.state.removeRoleArray.length !== 0 && showRemoveArray}
                                    </CardContent>
                                </Card>
                                <CardActions>
                                    {roleManagement}
                                    {showBtnGroup}
                                </CardActions>
                            </Grid>
                            <Grid item xs={5} lg={4} xl={4}>
                                {this.role.includes("ADMIN") &&
                                    <Card variant="outlined" style={{ 'padding': '15px', 'paddingTop': '5px', 'margin': '2%' }}>
                                        {addRoleInput}
                                    </Card>
                                }
                                <Card variant="outlined" style={{ 'padding': '15px', 'margin': '2%' }}>
                                    <FormControl component="fieldset">
                                        <FormLabel component="legend">{translations.user.user_roles}</FormLabel>
                                        <FormGroup>
                                            {showFormGroup}
                                        </FormGroup>
                                    </FormControl>
                                </Card>


                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            );
        } else {
            window.alert("Unauthorized");
            window.location.href = "/spaces";
        }
    }
}


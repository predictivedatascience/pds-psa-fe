import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { Email, EmailRecipient } from "../../../model/props";
import IconButton from "@material-ui/core/IconButton";
import Edit from "@material-ui/icons/Edit";
import DeleteOutline from "@material-ui/icons/DeleteOutline";
import Check from "@material-ui/icons/Check";
import Clear from "@material-ui/icons/Clear";
import { Add } from "@material-ui/icons";
import { TextField } from "@material-ui/core";
import { LanguageContext } from "../../../context/LanguageContext"

const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
});

function Row(props: any) {
    const [row, setRow] = React.useState<EmailRecipient>(props.row)
    const [tempRow, setTempRow] = React.useState<EmailRecipient>(props.row)
    const [editable, setEditable] = React.useState<boolean>(props.editable)

    const editRow = async (e: any, cell: string) => {
        let newRow = JSON.parse(JSON.stringify(tempRow));
        // @ts-ignore
        newRow[cell] = e.target.value;
        // await setRow(newRow)
        await setTempRow(newRow);
    }
    const saveRow = async () => {
        await setEditable(false);
        await props.save(tempRow)
        // await setRow(tempRow);
    }

    return (
        <TableRow key={row.id}>
            <TableCell component="th" scope="row">
                {!editable ? row.name :
                    <TextField value={tempRow.name} multiline fullWidth
                        onChange={async (e) => {
                            await editRow(e, 'name')
                        }} />}
            </TableCell>
            <TableCell>
                {!editable ? row.mail :
                    <TextField value={tempRow.mail} multiline fullWidth
                        onChange={async (e) => {
                            await editRow(e, 'mail')
                        }} />}
            </TableCell>
            {props.editMode &&
                <TableCell>
                    {!editable ?
                        <React.Fragment>
                            <IconButton onClick={() => {
                                setEditable(!editable);
                                setTempRow(JSON.parse(JSON.stringify(row)));
                            }}><Edit /></IconButton>
                            <IconButton onClick={() => {
                                props.delete(row)
                            }}><DeleteOutline /></IconButton>

                        </React.Fragment> :
                        <React.Fragment>
                            <IconButton
                                onClick={saveRow}
                            >
                                <Check />
                            </IconButton>
                            {row.name && row.name !== "" ?
                                <IconButton onClick={() => {
                                    setEditable(false)
                                }}
                                >
                                    <Clear />
                                </IconButton> :
                                <IconButton onClick={() => {
                                    props.delete(row)
                                }}
                                >
                                    <Clear />
                                </IconButton>
                            }
                        </React.Fragment>
                    }
                </TableCell>
            }
        </TableRow>
    )
}

export default function BasicTable(props: any) {
    const classes = useStyles();
    const [rows, setRows] = React.useState<EmailRecipient[]>(props.rows)
    const emptyRow: EmailRecipient = { name: "", mail: "" }
    const localization = React.useContext(LanguageContext)

    const deleteRow = async (row: EmailRecipient) => {
        let newRows = [...rows]
        if (!row.id) {
            newRows.splice(0, 1)
            setRows(newRows)
        } else {
            let rowToDelete = await newRows.find(source => source.id === row.id)
            newRows.splice(newRows.indexOf(rowToDelete!), 1)
        }
        // await setRows(newRows)
        await props.saveFunction({ target: { value: newRows } }, 'recipients')
    }

    const saveRow = async (row: EmailRecipient) => {
        let newRows = [...rows]
        if (!row.id) {
            row.id = row.name.toLocaleLowerCase().replace(" ", "_") + new Date().toISOString()
            newRows.splice(0, 1, row)
            setRows(newRows)
        } else {
            let rowToDelete = await newRows.find(source => source.id === row.id)
            newRows.splice(newRows.indexOf(rowToDelete!), 1, row)
        }
        // await setRows(newRows)
        await props.saveFunction({ target: { value: newRows } }, 'recipients')
    }
    //console.log(rows)
    return (
        <Table size="small" className={classes.table} aria-label="users">
            <TableHead>
                <TableRow>
                    <TableCell>{localization.translations.user.name}</TableCell>
                    <TableCell>{localization.translations.email.addresse} </TableCell>
                    <TableCell align="center">
                        {props.editMode && rows.indexOf(emptyRow) === -1 &&
                            <IconButton onClick={() => { let newRows = [...rows]; newRows.splice(0, 0, emptyRow); setRows(newRows) }}>
                                <Add />
                            </IconButton>
                        }
                    </TableCell>
                </TableRow>
            </TableHead>
            <TableBody>
                {rows && rows.map((row) => (
                    <Row key={row.id! + new Date()} row={row} editMode={props.editMode} editable={!row.name} save={saveRow} delete={deleteRow} />
                ))}
            </TableBody>
        </Table>
    );
}
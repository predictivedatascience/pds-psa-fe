import React, {Component} from "react";
import {ConfirmDialogProps, Email, EmailsDashboardState,} from "../../../model/props";
import {Grid, Paper, Typography,} from "@material-ui/core";
import {getUserRole} from "../../../controller/RoleController";
import {EmailSettingsFactory} from "../../../controller/factory/emailSettingsFactory/EmailSettingsFactory";
import {MySnackbarContentWrapper} from "../../components/PDSSnackbar";
import ConfirmDialog from "../../components/ConfirmDialog";
import {CollapsibleTable} from "./EmailSettingsTable";
import {EmailComponent} from "../../../controller/dataFetcher/emailDataFetcher";
import { LanguageContext } from "../../../context/LanguageContext";
import { Localization } from "../../../model/localization";

export class EmailSettingsDashboard extends Component<any,
    EmailsDashboardState> {
    role: string[];
    private emailSettingsFactory: EmailSettingsFactory;

    constructor(props: any) {
        super(props);

    this.state = {
      sort: "id_asc",
      isLoading: true,
      spaces: [] as Email[],
    };
    this.role = getUserRole();
    let emailDataProvider = new EmailComponent(this.props.config)
    this.emailSettingsFactory = new EmailSettingsFactory(
      emailDataProvider
    );
  }


  async componentDidMount() {
    await this.getData();
  }
  getData = async () => {
    try {
      let appState: EmailsDashboardState = await this.emailSettingsFactory.getEmailData(
        this.state.sort
      );
      appState.errorMsg = "";
      this.setState(appState);
      //console.log(appState);
    } catch (e) {
      this.setState({
        spaces: [],
        isLoading: false,
        errorMsg: `${this.context.translations.common.dashboard_cannot_be_loaded}: ${e}!`,
      });
    }
  };
  saveEmail = async (row: Email) => {
    try {
      if (row.id) {
        await this.emailSettingsFactory
          .saveEmailData(row)
          .then(() => setTimeout(this.getData, 1000));
      } else {
        await this.emailSettingsFactory
          .addEmailSpace(row)
          .then(() => setTimeout(this.getData, 1000));
      }
    } catch (e) {
      // await this.getData();
      await this.setState({ errorMsg: `${this.context.translations.email.row_cannot_be_saved}: ${e}!` });
    }
  };
  deleteEmail = async (row: Email) => {
    let deleteProps: ConfirmDialogProps = {
      title: `${this.context.translations.email.delete_space}`,
      message:
        this.context.translations.email.delete_confirmation + row.space + "?",
      onConfirm: () => {
        this.confirmDeleteEmail(row.space);
      },
      onClose: () => {
        this.setState({ deleteProps: {} as ConfirmDialogProps });
      },
      open: true,
    };
    await this.setState({ deleteProps: deleteProps });
  };
  confirmDeleteEmail = async (spaceName: string) => {
    let appState: EmailsDashboardState;
    try {
      await this.emailSettingsFactory
        .deleteSpace(spaceName)
        .then(() => setTimeout(this.getData, 1000));
    } catch (e) {
      // await this.getData();
      await this.setState({ errorMsg: `${this.context.translations.email.row_cannot_be_delete}` });
    }
  };

  setSort = async (sort: "space" | "subject" | "body") => {
    const oldSort = this.state.sort;
    let newSort = "";
    //console.log(oldSort, sort);
    if (oldSort.indexOf(sort) === -1) {
      newSort = sort + "_asc";
    } else if (oldSort === sort + "_asc") {
      newSort = sort + "_desc";
    } else {
      newSort = "id_asc";
    }
    await this.setState({ sort: newSort });
    await this.getData();
  };

  static contextType = LanguageContext
  render() {
    const translations: Localization = this.context.translations;

    let filteredData = [] as Email[]
    this.state.spaces.map((item: any) => {
      if (this.role.includes("ADMIN")) {
        return filteredData.push(item)
      } else if (this.role.length === 1) {
        return
      } else if (this.role.length > 1) {
        if (this.role.includes("EMAIL_CONFIG")) {
          this.role.map((j: any) => {
            if (j === "QSAUDIT") {
              j = "AUDIT"
            };
            let roleIsIncl = item.space.search(j.toLowerCase())
            if (roleIsIncl !== -1) {
              filteredData.push(item)

            }
          })

        }
      }

    })
    //console.log(filteredData)
    let errorMsgBar = (
      <MySnackbarContentWrapper
        open={!!this.state.errorMsg}
        variant="error"
        onClose={() => {
          this.setState({ errorMsg: "" });
        }}
        message={this.state.errorMsg}
      />
    );
    let confirmDialog = (
      <ConfirmDialog
        key={new Date().getTime()}
        title={this.state.deleteProps?.title!}
        message={this.state.deleteProps?.message!}
        onConfirm={this.state.deleteProps?.onConfirm!}
        onClose={this.state.deleteProps?.onClose!}
        open={!!this.state.deleteProps?.open}
      />
    );

    if (this.role.includes("ADMIN") || this.role.includes("EMAIL_CONFIG")) {
      return (
        <Grid container justify="center">

          <Grid item xs={12}>
            {confirmDialog}
            {errorMsgBar}
            <Paper
              variant={"outlined"}
              className={"dashboardHeader"}
              style={{ margin: "5px 3px 25px 3px" }}
            >
              <Grid container justify="center">
                <Grid item xs={10}>
                  <Typography
                    variant="h5"
                    component="h3"
                    style={{ textAlign: "center", marginLeft: "5px" }}
                  >
                    {translations.email.email_einstellungen}
                  </Typography>
                </Grid>
              </Grid>
            </Paper>
          </Grid>
          <Grid item xs={12} md={12} lg={12}>
            <CollapsibleTable
              {...this.props}
              key={new Date().getTime()}
              data={filteredData}
              saveFunction={this.saveEmail}
              deleteFunction={this.deleteEmail}
              sortFunction={this.setSort}
              sort={this.state.sort}
              role={this.role}
              dataFetcher={this.props.dataFetcher}
            />
          </Grid>
        </Grid>
      );
    } else {
      window.alert("Unauthorized");
      window.location.href = "/spaces";
    }
  }
}

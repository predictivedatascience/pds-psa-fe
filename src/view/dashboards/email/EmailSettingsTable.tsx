import { makeStyles } from "@material-ui/core/styles";
import { Email, EmailAttachment, EmailTableProps } from "../../../model/props";
import React, { forwardRef, useEffect } from "react";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import IconButton from "@material-ui/core/IconButton";
import DateRangeOutlinedIcon from '@material-ui/icons/DateRangeOutlined';
import KeyboardArrowUpIcon from "@material-ui/icons/KeyboardArrowUp";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import { Paper, TextField, Typography } from "@material-ui/core";
import Edit from "@material-ui/icons/Edit";
import DeleteOutline from "@material-ui/icons/DeleteOutline";
import Check from "@material-ui/icons/Check";
import Clear from "@material-ui/icons/Clear";
import Collapse from "@material-ui/core/Collapse";
import Box from "@material-ui/core/Box";
import UserSettingsTable from "./UserSettingsTable";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableBody from "@material-ui/core/TableBody";
import TableContainer from "@material-ui/core/TableContainer";
import { Add, UnfoldMore } from "@material-ui/icons";
import { Icons } from "material-table";
import AddBox from "@material-ui/icons/AddBox";
import ChevronRight from "@material-ui/icons/ChevronRight";
import SaveAlt from "@material-ui/icons/SaveAlt";
import FilterList from "@material-ui/icons/FilterList";
import FirstPage from "@material-ui/icons/FirstPage";
import LastPage from "@material-ui/icons/LastPage";
import ChevronLeft from "@material-ui/icons/ChevronLeft";
import Search from "@material-ui/icons/Search";
import ArrowDownward from "@material-ui/icons/ArrowDownward";
import Remove from "@material-ui/icons/Remove";
import ViewColumn from "@material-ui/icons/ViewColumn";
import emailDataPicker from "./emailDataPicker";
import EmailDataPicker from "./emailDataPicker";
import { LanguageContext } from "../../../context/LanguageContext"

import { EmailSettingsFactory } from "../../../controller/factory/emailSettingsFactory/EmailSettingsFactory";
import { DataFetcher } from "../../../controller/dataFetcher/DataFetcher";



const useRowStyles = makeStyles({
    root: {
        '& > *': {
            borderBottom: 'unset',
        },
    },
});

const emptyRow: Email = { space: "", subject: "", body: "", recipients: [], sender: "", attachment: {} as EmailAttachment }

function Row(props: { row: Email, editable?: boolean, delete: any, save: any, role: any, dataFetcher: any }) {




    const [row, setRow] = React.useState<Email>(props.row);
    const [tempRow, setTempRow] = React.useState<Email>(emptyRow)
    const [tempAttachment, setTempAttechment] = React.useState<EmailAttachment>({} as EmailAttachment)
    const [open, setOpen] = React.useState(false);
    const [editable, setEditable] = React.useState(!!props.editable);
    const [role, setRole] = React.useState(props.role)
    const classes = useRowStyles();
    const [calendarOpen, setCalendarOpen] = React.useState(false);
    const [appState, setState] = React.useState();
    const [errorMsg, setErrorMsg] = React.useState("");
    const localization = React.useContext(LanguageContext)

    //console.log(editable)
    const editAttachment = async (e: any, cell: string) => {
        if (role.includes("ADMIN")) {
            let newAttachment = JSON.parse(JSON.stringify(tempAttachment));
            // @ts-ignore
            newAttachment[cell] = e.target.value;
            await setTempAttechment(newAttachment);
            await editRow({ target: { value: newAttachment } }, 'attachment')
        } else if (cell === 'send_as') {
            let newAttachment = JSON.parse(JSON.stringify(tempAttachment));
            // @ts-ignore
            newAttachment[cell] = e.target.value;
            await setTempAttechment(newAttachment);
            await editRow({ target: { value: newAttachment } }, 'attachment')
        }
    }

    const editRow = async (e: any, cell: string) => {
        let newRow = JSON.parse(JSON.stringify(tempRow));
        // @ts-ignore
        newRow[cell] = e.target.value;
        await setTempRow(newRow);
    }
    const saveRow = async () => {
        await props.save(tempRow)
        setRow(tempRow);
        setEditable(false);
    }
    const isAdmin = role.includes("ADMIN");

    const isCalendarOpen = () => {
        setCalendarOpen(!calendarOpen)
    }

    return (
        <React.Fragment>


            {calendarOpen ? <EmailDataPicker calendarOpen={calendarOpen} isCalendarOpen={isCalendarOpen} row={row} dataFetcher={props.dataFetcher} /> : null}
            <TableRow className={classes.root}>
                <TableCell>
                    <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
                        {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                    </IconButton>
                </TableCell>
                {isAdmin ? (<TableCell component="th" scope="row">
                    {!editable ? row.space :
                        <TextField value={tempRow.space} multiline fullWidth
                            onChange={(e) => { editRow(e, 'space') }

                            }
                        />}
                </TableCell>) : (<TableCell component="th" scope="row">
                    {!editable ? row.space :
                        <TextField value={tempRow.space} multiline fullWidth disabled
                        /* onChange={(e)=>{editRow(e,'space')}} */

                        />}
                </TableCell>)}
                <TableCell>
                    {!editable ? row.subject :
                        <TextField value={tempRow.subject} multiline fullWidth
                            onChange={(e) => { editRow(e, 'subject') }} />}
                </TableCell>
                <TableCell>
                    {!editable ? row.body :
                        <TextField
                            value={tempRow.body}
                            multiline fullWidth
                            onChange={(e) => { editRow(e, 'body') }} />}
                </TableCell>
                <TableCell align={"center"}>{row.recipients.length}</TableCell>
                <TableCell>
                    {!editable ? row.sender :
                        <TextField value={tempRow.sender} multiline fullWidth
                            onChange={(e) => { editRow(e, 'sender') }} />}
                </TableCell>
                <TableCell align={"center"}>{row.attachment.file_name ? "yes" : "no"}</TableCell>
                <TableCell>
                    {!editable ?
                        <React.Fragment>
                            <IconButton onClick={() => {
                                setEditable(!editable);
                                setTempRow(JSON.parse(JSON.stringify(row)));
                                setTempAttechment({ ...row.attachment })
                            }}><Edit /></IconButton>
                            <IconButton onClick={() => {
                                props.delete(row)
                            }}><DeleteOutline /></IconButton>
                            <IconButton onClick={() => {
                                isCalendarOpen()

                            }}><DateRangeOutlinedIcon /></IconButton>

                        </React.Fragment> :
                        <React.Fragment>
                            <IconButton
                                onClick={saveRow}>
                                <Check />
                            </IconButton>
                            {row.id && row.id !== "" ?
                                <IconButton onClick={() => {
                                    setEditable(false)
                                }}
                                >
                                    <Clear />
                                </IconButton> :
                                <IconButton onClick={() => {
                                    props.delete(row)
                                }}
                                >
                                    <Clear />
                                </IconButton>
                            }
                        </React.Fragment>
                    }
                </TableCell>
            </TableRow>
            <TableRow>
                <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
                    <Collapse in={open} timeout="auto" unmountOnExit>
                        <Box margin={1}>
                            <Typography variant="h6" gutterBottom component="div">
                                {localization.translations.email.empfanger}
                            </Typography>
                            <UserSettingsTable key={new Date().toString()} rows={!editable ? row.recipients : tempRow.recipients} editMode={editable!!} saveFunction={editRow} />
                        </Box>
                        {(row.attachment.file_name || editable) &&
                            <Box margin={1}>
                                <Typography variant="h6" gutterBottom component="div">
                                    {localization.translations.email.anhange}
                                </Typography>
                                <Table size="small" aria-label="attachment">
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>{localization.translations.email.dateiname}</TableCell>
                                            <TableCell> {localization.translations.email.speicherort}</TableCell>
                                            <TableCell>{localization.translations.email.dateiendung}</TableCell>
                                            <TableCell>{localization.translations.email.verschicken_als}</TableCell>
                                            {editable &&
                                                <TableCell />
                                            }
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {!editable ?
                                            <TableRow key={row.attachment.file_name}>
                                                <TableCell component="th" scope="row">
                                                    {row.attachment.file_name}
                                                </TableCell>
                                                <TableCell>{row.attachment.file_path}</TableCell>
                                                <TableCell>{row.attachment.extension}</TableCell>
                                                <TableCell>{row.attachment.send_as}</TableCell>
                                            </TableRow> :
                                            <TableRow key="new">
                                                <TableCell component="th" scope="row">
                                                    <TextField value={tempAttachment.file_name} disabled={!isAdmin}
                                                        onChange={(e) => editAttachment(e, 'file_name')} />
                                                </TableCell>
                                                <TableCell>
                                                    <TextField value={tempAttachment.file_path} disabled={!isAdmin}
                                                        onChange={(e) => editAttachment(e, 'file_path')}
                                                    />
                                                </TableCell>
                                                <TableCell>
                                                    <TextField value={tempAttachment.extension} disabled={!isAdmin}
                                                        onChange={(e) => editAttachment(e, 'extension')}
                                                    />
                                                </TableCell>
                                                <TableCell>
                                                    <TextField value={tempAttachment.send_as}
                                                        onChange={(e) => editAttachment(e, 'send_as')}
                                                    />
                                                </TableCell>
                                            </TableRow>
                                        }
                                    </TableBody>
                                </Table>
                            </Box>
                        }
                    </Collapse>
                </TableCell>
            </TableRow>
        </React.Fragment>
    );
}


export function CollapsibleTable(props: EmailTableProps) {
    const [newRow, setNewRow] = React.useState<boolean>(false)
    const [rows, setRows] = React.useState<Email[]>(props.data)
    const [sort, setSort] = React.useState<string>("space")
    const [role, setRole] = React.useState(props.role)
    const localization = React.useContext(LanguageContext)

    const deleteRow = (row: Email) => {
        let newRows = [...rows]
        if (!(row.id || row.id === "0")) {
            newRows.splice(0, 1)
            setRows(newRows)
        } else {
            props.deleteFunction(row)
        }
    }

    const saveRow = (row: Email) => {
        props.saveFunction(row)
    }
    //console.log(props)
    return (
        <Paper variant={"outlined"} >
            <Table aria-label="collapsible table">
                <TableHead>
                    <TableRow>
                        <TableCell />
                        <TableCell style={{ minWidth: "150px" }}>
                            {localization.translations.common.space}
                            <IconButton onClick={(e) => { setSort('space'); props.sortFunction('space') }}>
                                {props.sort.indexOf("space") === -1 ? <UnfoldMore /> : (props.sort.indexOf("asc") !== -1 ? <KeyboardArrowDownIcon /> : <KeyboardArrowUpIcon />)}
                            </IconButton>
                        </TableCell>
                        <TableCell style={{ minWidth: "150px" }}>
                            {localization.translations.schraubdaten.betreff}
                            <IconButton onClick={(e) => { setSort('subject'); props.sortFunction('subject') }}>
                                {props.sort.indexOf("subject") === -1 ? <UnfoldMore /> : (props.sort.indexOf("asc") !== -1 ? <KeyboardArrowDownIcon /> : <KeyboardArrowUpIcon />)}
                            </IconButton>
                        </TableCell>
                        <TableCell style={{ minWidth: "150px" }}>{localization.translations.email.nachrichtentext}
                            <IconButton onClick={(e) => { setSort('body'); props.sortFunction('body') }}>
                                {props.sort.indexOf("body") === -1 ? <UnfoldMore /> : (props.sort.indexOf("asc") !== -1 ? <KeyboardArrowDownIcon /> : <KeyboardArrowUpIcon />)}
                            </IconButton>
                        </TableCell>
                        <TableCell align={"center"} style={{ minWidth: "150px" }}>
                            {localization.translations.email.empfanger}
                            <IconButton onClick={(e) => { setSort('recipients'); props.sortFunction('recipients') }}>
                                {props.sort.indexOf("recipients") === -1 ? <UnfoldMore /> : (props.sort.indexOf("asc") !== -1 ? <KeyboardArrowDownIcon /> : <KeyboardArrowUpIcon />)}
                            </IconButton>
                        </TableCell>
                        <TableCell align={"center"} style={{ minWidth: "180px" }}>
                            {localization.translations.email.absendername}
                            <IconButton onClick={(e) => { setSort('recipients'); props.sortFunction('recipients') }}>
                                {props.sort.indexOf("sender") === -1 ? <UnfoldMore /> : (props.sort.indexOf("asc") !== -1 ? <KeyboardArrowDownIcon /> : <KeyboardArrowUpIcon />)}
                            </IconButton>
                        </TableCell>
                        <TableCell align={"center"}>
                            {localization.translations.email.anhange}
                        </TableCell>
                        <TableCell align="center">
                            {rows.indexOf(emptyRow) === -1 &&
                                <IconButton onClick={() => { let newRows = [...rows]; newRows.splice(0, 0, emptyRow); setRows(newRows) }}>
                                    <Add />
                                </IconButton>
                            }
                        </TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>

                    {rows.map((row) => (
                        <Row key={row.space} row={row} delete={deleteRow} save={saveRow} editable={row == emptyRow} role={role} dataFetcher={props.dataFetcher} />
                    ))}
                </TableBody>
            </Table>
        </Paper>
    );
}

const tableIcons: Icons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
    DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
    ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
};

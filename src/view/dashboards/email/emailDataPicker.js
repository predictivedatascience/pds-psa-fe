import { useState, useEffect, useCallback } from 'react'
import React from 'react'
import MultipleDatesPicker from '@randex/material-ui-multiple-dates-picker';
import { EmailSettingsFactory } from "../../../controller/factory/emailSettingsFactory/EmailSettingsFactory";
import moment from "moment";
import { DataFetcher } from "../../../controller/dataFetcher/DataFetcher";
import { LanguageContext } from "../../../context/LanguageContext";

const EmailDataPicker = (props) => {
    const emailSettingsFactory = new EmailSettingsFactory(
        props.dataFetcher
    );

    const [open, setOpen] = useState(props.calendarOpen);
    const [date, setDates] = useState([])
    const [errorMsg, setErrorMsg] = useState("")

    let convertDate = date.map(item => new Date(item))
    const id = props.row.id
    const updateSkipDatesData = async (id, skip_dates) => {
        try {
            let appState = await emailSettingsFactory.updateEmailSkipDates(id, skip_dates);
            //console.log(appState)
            setOpen(false)
        } catch (e) {
            setErrorMsg(`${this.context.translations.common.dashboard_cannot_be_loaded}: ${e}!`)
        };
    };

    const getSkipDatesData = async (id) => {
        try {
            let appState = await emailSettingsFactory.getEmailSkipDates(id);
            setDates(appState.skip_dates);
        } catch (e) {
            setErrorMsg(`${this.context.translations.common.dashboard_cannot_be_loaded}: ${e}!`)
        }
    };

    useEffect(() => {
        getSkipDatesData(id);
    }, [])

    const onSubmit = useCallback(
        (dates) => {
            let newDates = dates.map(item => moment(item).format("YYYY-MM-DD"))
            //console.log(newDates)
            updateSkipDatesData(id, newDates)
            setDates(newDates)
            props.isCalendarOpen(false)
        },
        [setDates]
    )
    return (
        <>
            <MultipleDatesPicker
                open={props.calendarOpen}
                selectedDates={convertDate}
                onCancel={() => props.isCalendarOpen()}
                onSubmit={onSubmit}
            />
        </>
    )
}

export default EmailDataPicker
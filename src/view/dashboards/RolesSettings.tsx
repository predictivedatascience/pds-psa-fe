import React, { Component } from "react";
import RoleGroupController from "../../controller/RoleGroupController";
import { RolesSettingsState } from "../../model/props";
import { forwardRef } from "react";
import MaterialTable, { Icons } from "material-table";
import AddBox from "@material-ui/icons/AddBox";
import ArrowDownward from "@material-ui/icons/ArrowDownward";
import Check from "@material-ui/icons/Check";
import ChevronLeft from "@material-ui/icons/ChevronLeft";
import ChevronRight from "@material-ui/icons/ChevronRight";
import Clear from "@material-ui/icons/Clear";
import DeleteOutline from "@material-ui/icons/DeleteOutline";
import Edit from "@material-ui/icons/Edit";
import FilterList from "@material-ui/icons/FilterList";
import FirstPage from "@material-ui/icons/FirstPage";
import LastPage from "@material-ui/icons/LastPage";
import Remove from "@material-ui/icons/Remove";
import SaveAlt from "@material-ui/icons/SaveAlt";
import Search from "@material-ui/icons/Search";
import ViewColumn from "@material-ui/icons/ViewColumn";
import {
  Grid,
  GridList, IconButton, Input, InputAdornment, InputLabel,
  Paper,
  TablePagination,
  TextField,
  Typography,
} from "@material-ui/core";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import PersonOutlineIcon from "@material-ui/icons/PersonOutline";
import PeopleIcon from "@material-ui/icons/People";
import PersonAddIcon from "@material-ui/icons/PersonAdd";
import FolderOpenIcon from "@material-ui/icons/FolderOpen";
import { getUserRole } from "../../controller/RoleController";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import palette from "../../theme/palette";
import {theme} from "../../theme";
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import {Visibility, VisibilityOff} from "@material-ui/icons";
import { LanguageContext } from "../../context/LanguageContext";
import { Localization } from "../../model/localization";

const tableIcons: Icons = {
  Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
  Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
  Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
  DetailPanel: forwardRef((props, ref) => (
    <ChevronRight {...props} ref={ref} />
  )),
  Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
  Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
  Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
  FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
  LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
  NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  PreviousPage: forwardRef((props, ref) => (
    <ChevronLeft {...props} ref={ref} />
  )),
  ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
  SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
  ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
  ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
};
export interface rolesSettingsStateDashboard extends RolesSettingsState {
  openDialog: boolean;
  newUser: string;
  newUserPass: string;
  showPassword: boolean;
}

export class RolesSettings extends Component<any, rolesSettingsStateDashboard> {
  private roleGroupController: RoleGroupController;
  role: string;

  constructor(props: any) {
    super(props);

    this.handleClickNavigateToEditDashboard = this.handleClickNavigateToEditDashboard.bind(
      this
    );
    this.handleClickNavigateToDeleteDashboard = this.handleClickNavigateToDeleteDashboard.bind(
      this
    );
    this.handleClickOpen = this.handleClickOpen.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleAddNewUserClick = this.handleAddNewUserClick.bind(this);
    this.handleAddNewUserChange = this.handleAddNewUserChange.bind(this);

    this.state = {
      logged: {
        name: "",
        roles: [],
      },
      roles: [],
      all: [],
      isLoading: true,
      openDialog: false,
      newUser: "",
      newUserPass: "",
      showPassword: false,
    };
    this.role = getUserRole();
    this.roleGroupController = new RoleGroupController(this.props.dataFetcher);
  }

  async componentDidMount() {
    try {
      let appState: RolesSettingsState = await this.roleGroupController.showRole();
      this.setState(appState);
      //console.log(appState);
    } catch (e) {
      this.setState({
        all: [],
        isLoading: false,
        errorMsg: `${this.context.translations.common.dashboard_cannot_be_loaded}: ${e}!`,
      });
    }
  }

  private handleAddNewUserClick = async () => {
    try {
      let appState: RolesSettingsState = await this.roleGroupController.insertUser(
        this.state.newUser,
          this.state.newUserPass
      );
      this.setState(appState);
      this.setState({ openDialog: false });
      //console.log(appState);
      return (window.location.href = `/roles_settings/user/${this.state.newUser}/edit`);
    } catch (e) {
      this.setState({
        all: [],
        isLoading: false,
        errorMsg: `${this.context.translations.common.dashboard_cannot_be_loaded}: ${e}!`,
      });
    }
    //console.log("added user: " + this.state.newUser);
  };

  private handleAddNewUserChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    const newUserChange = event.target.value;
    this.setState({ newUser: newUserChange });
    //console.log(this.state.newUser); window.location.href = `/roles_settings/user/${rowData.name}/edit`
  };
  private handleAddUserPasswordChange = (
      event: React.ChangeEvent<HTMLInputElement>
  ) => {
    const newUserPassChange = event.target.value;
    this.setState({ newUserPass: newUserPassChange });
    //console.log(this.state.newUser); window.location.href = `/roles_settings/user/${rowData.name}/edit`
  };

  private handleClickNavigateToEditDashboard(rowData: any) {
    return (
        window.location.href = `/roles_settings/user/${rowData.name}/edit`
    );
  }
  private handleClickNavigateToDeleteDashboard(rowData: any) {
    return (window.location.href = `/roles_settings/user/${rowData.name}/delete`);
  }

  handleClickShowPassword = () => {
    this.setState({showPassword: !this.state.showPassword})
  };


  handleClickOpen = () => {
    this.setState({ openDialog: true });
  };

  handleClose = () => {
    this.setState({ openDialog: false });
  };
  static contextType = LanguageContext
  render() {
    const translations: Localization = this.context.translations;
    if (this.role.includes("MANAGE") || this.role.includes("ADMIN")) {
      let itemsPerPage: number[];
      if (this.state.all.length < 500) {
        if (this.state.all.length < 200) {
          if (this.state.all.length < 100) {
            if (this.state.all.length < 50) {
              itemsPerPage = [5, 10, 20];
            } else {
              itemsPerPage = [5, 10, 20, 50];
            }
          } else {
            itemsPerPage = [5, 10, 20, 100];
          }
        } else {
          itemsPerPage = [5, 10, 20, 100, 200];
        }
      } else {
        itemsPerPage = [5, 10, 20, 100, 200, 500];
      }
      const addNewUser = (
        <div>
          <div onClick={this.handleClickOpen}>
            <CardContent>
              <PersonAddIcon color={"primary"} />
            </CardContent>
          </div>

          <Dialog
            open={this.state.openDialog}
            onClose={this.handleClose}
            aria-labelledby="form-dialog-title"
            maxWidth={"xs"}
            fullWidth={true}
          >
            <DialogTitle id="form-dialog-title">{translations.user.add}</DialogTitle>
            <DialogContent>
              <div style={{padding: '15px'}} >
              <InputLabel htmlFor="userID">{translations.user.id}</InputLabel>
              <Input
                  fullWidth
                  id="userID"
                  type={ 'text'}
                  value={this.state.newUser}
                  onChange={this.handleAddNewUserChange}
              />
              </div>
              <div style={{padding: '15px'}} >
                <InputLabel htmlFor="password">Password</InputLabel>
                <Input
                    fullWidth

                    id="password"
                    type={this.state.showPassword ? 'text' : 'password'}
                    value={this.state.newUserPass}
                    onChange={this.handleAddUserPasswordChange}
                    endAdornment={
                      <InputAdornment position="end">
                        <IconButton
                            aria-label="toggle password"
                            onClick={this.handleClickShowPassword}
                            style={{marginBottom: '10px'}}
                        >
                          {this.state.showPassword ? <Visibility /> : <VisibilityOff />}
                        </IconButton>
                      </InputAdornment>
                    }
                />
              </div>

            </DialogContent>
            <DialogActions>
              <Button onClick={this.handleClose} color="primary">
                {translations.common.cancel}
              </Button>
              {this.state.newUser === "" || this.state.newUserPass === '' ? (
                <Button variant="text" disabled>
                  {" "}
                  {translations.common.add}{" "}
                </Button>
              ) : (
                <Button
                  onClick={this.handleAddNewUserClick}
                  variant="outlined"
                  color="primary"
                >
                  {translations.common.add}
                </Button>
              )}
            </DialogActions>
          </Dialog>
        </div>
      );

      return (
        <Grid container justify="center">
          <Grid item xs={12}>
            <Paper
              className={"dashboardHeader"}
              variant="outlined"
              style={{ margin: "5px 3px 25px 3px" }}
            >
              <Grid container justify="center">
                <Grid item xs={10}>
                  <Typography
                    variant="h5"
                    component="h3"
                    style={{ textAlign: "center", marginLeft: "5px" }}
                  >
                    {translations.user.role_settings}
                  </Typography>
                </Grid>
              </Grid>
            </Paper>
          </Grid>
          <Grid item xs={12} xl={10}>
            <Grid container direction="row" justify="space-evenly">
              <Grid item xs={5} lg={4} xl={4}>
                <GridList cellHeight={185} style={{ margin: "0 auto" }}>
                  <Card
                      variant="outlined"
                      style={{
                        width: "180px",
                        margin: "2%",
                        backgroundColor: theme.palette.primary.main,
                        color: "white",
                      }}
                  >
                    <CardContent>
                      <PeopleIcon />
                      <Typography gutterBottom style={{color: palette.primary.contrastText}}>Users</Typography>
                      <Typography variant="h4" component="h2" style={{color: palette.primary.contrastText}}>
                        {this.state.all.length}
                      </Typography>
                    </CardContent>
                  </Card>

                  <Card style={{ width: "180px", margin: "2%" }}
                        variant="outlined">
                    <CardContent>
                      <PersonOutlineIcon color={"primary"} />
                      <Typography color="primary" gutterBottom>
                        {translations.user.current_user}
                      </Typography>
                      <Typography color={"primary"} variant="h4" component="h2">
                        {this.state.logged.name}
                      </Typography>
                    </CardContent>
                  </Card>

                  <Card style={{ width: "180px", margin: "2%" }}
                        variant="outlined">
                    <CardContent>
                      <FolderOpenIcon color={"primary"} />
                      <Typography color={"primary"} gutterBottom>
                        {translations.user.user_roles}
                      </Typography>
                      <Typography color={"primary"} variant="h6" component="h2">
                        {this.state.logged.roles.map((i) => {
                          return <p key={i}>{i}</p>;
                        })}
                      </Typography>
                    </CardContent>
                  </Card>


                  <GridList
                    cellHeight={70}
                    style={{ marginLeft: "1%", marginTop: "1%" }}
                  >
                    {(this.role.includes("MANAGE") ||
                      this.role.includes("ADMIN")) && (
                      <Card
                          variant="outlined"
                          style={{ width: "80px", margin: "5px" }}
                          className="spaceHover"
                      >
                        {addNewUser}
                      </Card>
                    )}
                  </GridList>
                </GridList>
              </Grid>

              <Grid item xs={5} md={6} lg={7} xl={8}>
                <MaterialTable
                  title={translations.user.role_settings}
                  icons={tableIcons}
                  columns={[
                    { title: `${translations.user.name}`, field: "name" },
                    { title: `${translations.user.roles}`, field: "roles" },
                  ]}
                  data={this.state.all.map((i) => {
                    return {
                      name: i.name,
                      roles: i.roles.map((j) => {
                        return `${j} `;
                      }),
                    };
                  })}
                  actions={[
                    {
                      icon: () => <Edit color={"action"} />,
                      tooltip: `${translations.user.edit_roles}`,
                      onClick: (event, rowData) =>
                        this.handleClickNavigateToEditDashboard(rowData),
                    },
                    {
                        icon: () => <DeleteForeverIcon color={"action"} />,
                        tooltip: 'Delete User',
                        onClick: (event, rowData) => this.handleClickNavigateToDeleteDashboard(rowData)
                    }
                  ]}
                  options={{
                    sorting: true,
                    exportButton: true,
                    grouping: true,
                    exportAllData: true,
                    headerStyle: {
                      backgroundColor: "#D9D9D9",
                    },
                    // groupingCount: true,
                    //columnsButton: true,
                    emptyRowsWhenPaging: false,
                    actionsColumnIndex: -1,
                  }}
                  components={{
                    Pagination: (props) => (
                      <div>
                        <TablePagination
                          {...props}
                          rowsPerPageOptions={itemsPerPage}
                        />
                      </div>
                    ),
                  }}
                />
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      );
    } else {
      window.alert("Unauthorized");
      window.location.href = "/spaces";
    }
  }
}

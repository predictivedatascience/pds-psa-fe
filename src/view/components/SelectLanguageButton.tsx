import React from 'react';
import 'typeface-roboto';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Menu, { MenuProps } from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemText from '@material-ui/core/ListItemText';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';


import { LanguageContext } from '../../context/LanguageContext';
import { LanguageContexValue } from "../../model/localization";

// @ts-ignore
import { IconFlagUK, IconFlagDE } from 'material-ui-flags';
import {Chip} from "@material-ui/core";

const StyledMenu = withStyles({
    paper: {
        border: '1px solid #d3d4d5',
    },
})((props: MenuProps) => (
    <Menu
        elevation={0}
        getContentAnchorEl={null}
        anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
        }}
        transformOrigin={{
            vertical: 'top',
            horizontal: 'center',
        }}
        {...props}
    />
));

const StyledMenuItem = withStyles(theme => ({
    root: {
        padding: "0 10px",
        '&:focus': {
            backgroundColor: theme.palette.primary.main,
            '& .MuiListItemIcon-root, & .MuiListItemText-primary': {
                color: theme.palette.common.white,
            },
        },
    },
}))(MenuItem);


export default function SelectLanguageButton() {

    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
    const localization = React.useContext(LanguageContext);

    const handleClick = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const renderflag = (language: string) =>
        <div style={{ margin: "0 5px -6px 0" }}>
            {language == "EN" ? <IconFlagUK /> : <IconFlagDE />}
        </div>

    const renderItems = (items: string[]) => {
        return (
            items.map(item => (
                <StyledMenuItem
                    value={item}
                    key={item}
                    // @ts-ignore
                    onClick={() => { localization.handleLanguageChange(item); handleClose() }}>
                    {renderflag(item)}
                    <ListItemText primary={item} />
                </StyledMenuItem>
            ))
        )
    }

    const items = localization.language === "EN" ? ["DE", "EN"] : ["EN", "DE"]
    return (
        <React.Fragment>
            <div>
                <Chip
                    label={renderflag(localization.language)}
                    icon={<ArrowDropDownIcon/>}
                    onClick={handleClick}
                    onMouseOver={handleClick}
                    style={{textAlign: "center"}}
                />
                <StyledMenu
                    id="customized-menu"
                    anchorEl={anchorEl}
                    keepMounted
                    open={Boolean(anchorEl)}
                    onClose={handleClose}
                    MenuListProps={{onMouseLeave: handleClose}}
                >
                    {renderItems(items)}
                </StyledMenu>
            </div>
        </React.Fragment>
    );
}
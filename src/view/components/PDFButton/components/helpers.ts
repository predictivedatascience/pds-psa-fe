import html2canvas from "html2canvas";

interface PdfOffset {
    initial: number;
    increment: number;
}

export const  getPageCanvas = async (scale: number,scrollX: number, scrollY: number, windowWidth?: any, windowHeight?: any) =>
{
    window.scrollTo(0,0);
    let body =(document.getElementById("DashboardContainer"))!;
    // let body = document.createElement("div");
    // body.appendChild(originBody.cloneNode(true));
    // document.body.append(body);
    let canvas;
    if(windowWidth !== undefined && windowHeight !== undefined){
        canvas = await html2canvas(body, {
            ignoreElements: ((element) => (
                    element === document.getElementsByTagName("noscript")[0]) ||
                    element === document.getElementsByTagName("header")[0] ||
                    element === document.getElementById("hiddenDate3lvlAudit") ||
                    element === document.getElementById("QSMRdatePicker") ||
                    element === document.getElementById("QSMRdashboardHeaderWithButtons")
            ),
            foreignObjectRendering: true,
            allowTaint:true,
            useCORS:true,
            scale: scale,
            scrollX: scrollX,
            scrollY: scrollY,
            backgroundColor:null,
            width: windowWidth,
            height: windowHeight
        });
    }else{
        canvas = await html2canvas(body, {
            ignoreElements: ((element) => (
                    element === document.getElementsByTagName("noscript")[0]) ||
                    element === document.getElementsByTagName("header")[0] ||
                    element === document.getElementById("hiddenDate3lvlAudit") ||
                    element === document.getElementById("QSMRdatePicker") ||
                    element === document.getElementById("QSMRdashboardHeaderWithButtons")
            ),
            foreignObjectRendering: true,
            allowTaint:true,
            useCORS:true,
            scale: scale,
            scrollX: scrollX,
            scrollY: scrollY,
            windowWidth: document.documentElement.offsetWidth,
            windowHeight: document.documentElement.offsetHeight,
            backgroundColor:null,
        });
    }
    return canvas
};


export const getBreakMarks = (divsToPrint: HTMLElement[], pageHeight: number, pdfOffset: PdfOffset) => {
    let totalHeight = 0;
    let usedHeight = 0;
    let breakMarks = [] as number[];
    let offset = pdfOffset.initial;
    divsToPrint.forEach(div => {
        //console.log("client: " + div.clientHeight + "offset: " + div.offsetHeight + "scroll" + div.scrollHeight);
        let elementHeight = div.offsetHeight;
        if (usedHeight + elementHeight > (pageHeight - offset)) {
            breakMarks.push(totalHeight + offset);
            usedHeight = 0;
        }
        totalHeight += elementHeight;
        usedHeight += elementHeight;
        offset += pdfOffset.increment;
    });
    //console.log("total height: " + totalHeight);
    breakMarks.push(totalHeight + 65 + (pageHeight - usedHeight));
    return breakMarks;
};

export const splitCanvas = (parentCanvas: HTMLCanvasElement, a4Width: number, a4Height: number, factor: number) => {
    let dashboardContainer = document.getElementById("DashboardContainer")!;
    let innerDiv = dashboardContainer.children[0];

    //console.log("Height::" + a4Height + "Width:: " + a4Width);

    let divsToPrint = [].slice.call(innerDiv.children);
    let pdfOffset: PdfOffset = {initial: 65, increment: -8};
    //console.log(window.location.pathname);
    //console.log(window.location.pathname.indexOf("/qspr_dashboard/QSProduction") !== -1);
    if (window.location.pathname.indexOf("/qspr_dashboard/QSProduction") !== -1) {
        divsToPrint.pop();
        divsToPrint = [...divsToPrint, ...([].slice.call(innerDiv.children[innerDiv.children.length - 1].children[0].children))];
        pdfOffset = {initial: 75, increment: -0.1};
    }
    else if (window.location.pathname.indexOf("/monitoring") !== -1 || window.location.pathname.indexOf("/schraubdaten") !== -1 ) {
        divsToPrint.pop();
        divsToPrint.pop()
        let innerChildren= [].slice.call(innerDiv.children[innerDiv.children.length - 2].children)
        let i=-1
        // @ts-ignore
        divsToPrint = [...divsToPrint, ...innerChildren.filter(child=>{
            i++
            // @ts-ignore
            return !(child.clientWidth <= innerDiv.clientWidth/2 && i%2===1)
        })];
        pdfOffset = {initial:  window.location.pathname.indexOf("/schraubdaten") !== -1? 85: 90, increment: -0.1};
    }
    //console.log({divsToPrint:divsToPrint});
    const breakMarks: number[] = getBreakMarks(divsToPrint, Math.floor((a4Height - 20) / factor), pdfOffset);
    //console.log(breakMarks);
    // split the canvas into multiple small canvases
    let canvases = [] as HTMLCanvasElement[];
    let canvasStart = 65;
    breakMarks.forEach(breakMark => {
        let canvas = document.createElement("canvas");
        let ctx = canvas.getContext("2d")!;
        canvas.width = parentCanvas.width;
        canvas.height = breakMark - canvasStart;
        ctx.drawImage(parentCanvas, 0, canvasStart, canvas.width, canvas.height, 0, 30, canvas.width, canvas.height - 30);
        canvases.push(canvas);
        canvasStart = breakMark;
    });
    //console.log(window.location.pathname.substring(0,30))
    return canvases;
};
export const b64toBlob = (b64Data: any, contentType='', sliceSize=512) => {
    const byteCharacters = atob(b64Data);
    const byteArrays = [];

    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        const slice = byteCharacters.slice(offset, offset + sliceSize);

        const byteNumbers = new Array(slice.length);
        for (let i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }

        const byteArray = new Uint8Array(byteNumbers);
        byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, {type: contentType});
    return blob;
}


import * as React from "react";
import {Component} from "react";
import {Button, Chip, CircularProgress, Grid} from "@material-ui/core";
import html2canvas from "html2canvas";
import jsPDF from "jspdf";
import PictureAsPdfIcon from '@material-ui/icons/PictureAsPdf';
import { DataFetcher } from "../../../controller/dataFetcher/DataFetcher";
import { b64toBlob, getPageCanvas, splitCanvas } from "./components/helpers";
import { Configuration } from "../../../model/model";

interface PDFButtonProps {
    dataFetcher: DataFetcher,
    config: Configuration,
    line: string,
    modelKeyword: string,
    date?: string,

}

interface PDFButtonState {
    generating: boolean
}

export class PDFButton extends Component<PDFButtonProps, PDFButtonState> {
    constructor(props: PDFButtonProps) {
        super(props);
        this.state = {
            generating: false
        }
    }

    render = () => {
        return (
            <Grid
                container
                style={{paddingRight: "5px"}}
                id="pdfButton"
            >
                {
                    <Chip label={"PDF"}
                          icon={this.state.generating ?
                              <CircularProgress variant={"indeterminate"}
                               color={"inherit"}
                               size={"18px"}
                               disableShrink={true}/>
                          :
                              <PictureAsPdfIcon/>
                          }
                          //disabled={this.state.generating}
                          onClick={() => {
                              this.setState({generating: true});
                              this.createPdf().then(() => {
                                  this.setState({generating: false})
                              });
                          }}
                    />
                }
            </Grid>
        );
    };

    createPdf = async () => {
        let urlParse = window.location.pathname.split('/')

       switch (true) {
           default: {
               let parentCanvas;
               if((urlParse[1] === "qsmr_dashboard" && urlParse.length === 2) || (urlParse[1] === "qsmr_id_dashboard" && urlParse.length === 2) ){
                   parentCanvas = await getPageCanvas(1.4, 95, 90, window.innerWidth,window.innerHeight * 1.4);
               }else if((urlParse[1] === "qsmr_id_dashboard" && urlParse[2] === "detail" && urlParse.length === 3)){
                   parentCanvas = await getPageCanvas(1.05, 90, 63);
               }else{
                   parentCanvas = await getPageCanvas(1.35, 90, 78);
               }
               let pdf: any;
               if (window.location.pathname.indexOf("/qspr_dashboard/QSProduction") !== -1 || window.location.pathname.indexOf("schraubdaten") !== -1 ) {
                   pdf  = new jsPDF("p", 'px', 'a4');
               }else {
                   pdf = new jsPDF("l", 'px', 'a4');
               }
               pdf.internal.scaleFactor = 1.7;
               const a4Width = pdf.internal.pageSize.getWidth();
               const a4Height = pdf.internal.pageSize.getHeight();
               const factor = a4Width / (parentCanvas.width);
               const canvases: HTMLCanvasElement[] = splitCanvas(parentCanvas, a4Width, a4Height, factor);

                canvases.forEach(canvas => {
                    pdf.addPage("a4", "l");
                    let imgData = canvas.toDataURL();
                    pdf.addImage(imgData, 'PNG', 10, 0, canvas.width * factor - 20, canvas.height * factor, {}, "Slow");
                    let footer = new Image();
                    footer.src = "/img/footer.png";
                    pdf.addImage(footer, "PNG", 0, a4Height - 40);
                    pdf.setFontSize(7);
                    pdf.setTextColor(90, 90, 120,);
                    let dateOptions = {year: 'numeric', month: 'numeric', day: 'numeric'};
                    pdf.text(new Date().toLocaleDateString("de-DE", dateOptions), 189, a4Height - 17, null, null, "center");
                    pdf.text("IT-Projekthaus(SIP)", 137, a4Height - 17, null, null, "center")
                });
                pdf.deletePage(1);
                pdf.save("download.pdf");
                break;
            }
        }
    };
}
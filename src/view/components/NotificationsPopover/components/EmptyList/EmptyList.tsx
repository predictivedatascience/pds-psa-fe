import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import {Theme} from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        textAlign: 'center',
        padding: theme.spacing(0)
    },
    image: {
        height: 60,
        backgroundImage: 'url("/images/undraw_empty_xct9.svg")',
        backgroundPositionX: 'right',
        backgroundPositionY: 'center',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover'
    }
}));

const EmptyList = (props: { [x: string]: any; className: any; }) => {
    const { className, ...rest } = props;

    const classes = useStyles();

    return (
        <div
            {...rest}
            className={clsx(classes.root, className)}
        >
            {props.child}
        </div>
    );
};

EmptyList.propTypes = {
    className: PropTypes.string
};

export default EmptyList;

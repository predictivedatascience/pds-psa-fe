import React from "react"
import { Link as RouterLink } from "react-router-dom"
import PropTypes from "prop-types"
import clsx from "clsx"
import moment from "moment"
import { makeStyles } from "@material-ui/styles"
import {
  Avatar,
  Button,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  Theme,
} from "@material-ui/core"
import ArrowForwardIcon from "@material-ui/icons/ArrowForward"
import PaymentIcon from "@material-ui/icons/Payment"
import PeopleIcon from "@material-ui/icons/PeopleOutlined"
import CodeIcon from "@material-ui/icons/Code"
import StoreIcon from "@material-ui/icons/Store"

const useStyles = makeStyles((theme: Theme) => ({
  root: {},
  listItem: {
    "&:hover": {
      backgroundColor: theme.palette.background.default,
    },
  },
  /*arrowForwardIcon: {
        color: theme.palette.icon
    }*/
}))

const NotificationList = (props: {
  [x: string]: any
  notifications: any
  className: any
}) => {
  const { notifications, className, ...rest } = props

  const classes = useStyles()

  const avatars = {
    order: (
      <Avatar>
        <PaymentIcon />
      </Avatar>
    ),
    user: (
      <Avatar>
        <PeopleIcon />
      </Avatar>
    ),
    project: (
      <Avatar>
        <StoreIcon />
      </Avatar>
    ),
    feature: (
      <Avatar>
        <CodeIcon />
      </Avatar>
    ),
  }

  return (
    <List {...rest} className={clsx(classes.root, className)} disablePadding>
      {notifications.map(
        (
          notification: {
            id: string | number | undefined
            type: React.ReactText
            title: React.ReactNode
            created_at:
              | string
              | number
              | void
              | moment.Moment
              | Date
              | React.ReactText[]
              | moment.MomentInputObject
              | undefined
          },
          i: number
        ) => (
          <ListItem
            className={classes.listItem}
            component={RouterLink}
            divider={i < notifications.length - 1}
            key={notification.id}
            to="#"
          >
            <ListItemText
              primary={notification.title}
              primaryTypographyProps={{ variant: "body1" }}
              //@ts-ignore
              secondary={moment(notification.created_at).fromNow()}
            />

            <ArrowForwardIcon />
          </ListItem>
        )
      )}
    </List>
  )
}

export default NotificationList

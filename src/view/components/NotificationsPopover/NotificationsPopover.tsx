import React from 'react';
//import { Link as RouterLink } from 'react-router-dom';
import { makeStyles } from '@material-ui/styles';
import {
    Popover,
    Divider,
    colors
} from '@material-ui/core';
import EmptyList from "./components/EmptyList";


const useStyles = makeStyles(() => ({
    root: {
        width: 225,
        //maxWidth: '100%'
    },
    actions: {
        backgroundColor: colors.grey[50],
        justifyContent: 'center'
    }
}));

const NotificationsPopover = (props: any) => {
    const { notifications, anchorel, ...rest } = props;

    const classes = useStyles();
    return (
        <Popover
            {...rest}
            anchorEl={anchorel}
            open={props.open}
            onClick={props.onMouseEnter}
            onMouseEnter={props.onMouseEnter}
            onMouseLeave={props.onMouseLeave}
            anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'center'
            }}
        >
            <div className={classes.root} onClick={props.onMouseLeave}>
                    <EmptyList {...props} className={''}/>
                <Divider />
            </div>
        </Popover>
    );
};


export default NotificationsPopover;

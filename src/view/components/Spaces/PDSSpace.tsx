import { Grid, Typography } from "@material-ui/core";
import * as React from "react";
import 'typeface-roboto';
import { SpaceCard } from "../PDSCard";
import { Link as RouterLink } from "react-router-dom";
import { SpaceGridStructure } from "../../../model/model";
import { LanguageContext } from "../../../context/LanguageContext";

export function PDSSpace(props:any) {
    const localization= React.useContext(LanguageContext)
    const spacesList: SpaceGridStructure[] = [
        {
            hidden: false,
            href: "/spaces",
            title: localization.translations.space.no_spaces_available,
            icon: "/img/prod.svg"
        }
    ]

    return (
        <React.Fragment>
            <div style={{
                backgroundImage: "url(/img/wallpaper-vw.jpg)",
                backgroundSize: "cover", backgroundRepeat: "no-repeat",
                //backgroundColor: '#F0F2F5',
                width: '100vw', height: '100vh',
                position: "fixed", left: 0, top: 0, zIndex: -1
            }}
            />
            <div className="spaceVisibility" style={{ padding: "20px", margin: "0 auto" }}>
                <div style={{ margin: "15px 0", flexGrow: 1 }}>
                    <Grid
                        container
                        direction="row"
                        justify="center"
                        alignItems="center"
                        style={{ paddingBottom: '10px' }}
                    >
                        <img src="/img/pds_logo_short.png" alt="PDS logo" height="60"/>
                    </Grid>

                    <Typography variant="h3" component="h5" align="center">
                        {localization.translations.space.select_your_space}
                    </Typography>
                </div>
            </div>

            <div className="spaceVisibility" style={{ padding: "20px" }}>
                <Grid container
                    direction="row"
                    justify="center"
                    alignItems="center">
                    <Grid spacing={5} item xs={12} lg={10} container
                        direction="row"
                        justify="center"
                        alignItems="center"
                    >
                        {spacesList.map((i) => {
                            return (
                                <Grid key={i.title} item lg={3} style={{ maxWidth: "250px" }} hidden={i.hidden}>
                                    <RouterLink to={`${i.href}`} style={{ borderRadius: '2px' }} className="spaceHover">
                                        <SpaceCard title={i.title} icon={i.icon} />
                                    </RouterLink>
                                </Grid>
                            )
                        })}
                    </Grid>
                </Grid>
            </div>
        </React.Fragment>
    )
}

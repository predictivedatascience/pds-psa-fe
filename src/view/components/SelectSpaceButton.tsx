import * as React from "react";
import 'typeface-roboto';
import { withStyles } from '@material-ui/core/styles';
import Menu, { MenuProps } from '@material-ui/core/Menu';
import ListItemText from '@material-ui/core/ListItemText';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import {getUserRole} from "../../controller/RoleController";
import {SpaceMenuItemStructure} from "../../model/model";
import drawerConfig from "../../layouts/Dashboard/components/NavBar/components/drawerConfig";
import {Link as RouterLink, Link} from "react-router-dom";
import {Chip, Divider, List, ListItem, ListItemIcon} from "@material-ui/core";
import EmailIcon from "@material-ui/icons/Email";


const StyledMenu = withStyles({
    paper: {
        border: '1px solid #d3d4d5',
    },
})((props: MenuProps) => (
    <Menu
        elevation={0}
        getContentAnchorEl={null}
        anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
        }}
        transformOrigin={{
            vertical: 'top',
            horizontal: 'center',
        }}
        {...props}
    />
));


export function SelectSpaceButton(props:any) {
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

    const handleClick = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    let role = getUserRole();
    let getRouteID = window.location.pathname.replace(/^\/([^\/]*).*$/, '$1')
    let drawerID = getRouteID as keyof typeof drawerConfig;

    let menuItem: SpaceMenuItemStructure[] = [
        {
            hidden: false,
            href: "/spaces",
            title: "Spaces",
            icon: "/img/space.svg"
        }
    ]

    return (
        <React.Fragment>
            <div>
                <Chip
                    //label={drawerConfig[drawerID] ? drawerConfig[drawerID].title : 'space'}
                    label={'SPACE'}
                    icon={<ArrowDropDownIcon/>}
                    onClick={handleClick}
                    onMouseOver={handleClick}
                    style={{textAlign: "center"}}
                />
                <StyledMenu
                    id="customized-menu"
                    anchorEl={anchorEl}
                    keepMounted
                    open={Boolean(anchorEl)}
                    onClose={handleClose}
                    MenuListProps={{onMouseLeave: handleClose}}
                >
                    <List style={{padding: 0, margin: 0}} >
                    {window.location.pathname !== '/spaces' && menuItem.map((i)=>{
                        if(i.hidden===false){
                            return(
                                <>
                                    <ListItem button
                                        //style={{display: i.hidden, }}
                                              component={RouterLink} to={i.href}>
                                        <ListItemIcon>
                                            <img alt={i.title} src={i.icon} width={20} height={20}/>
                                        </ListItemIcon>
                                        <ListItemText primary={i.title} style={{fontStyle: '10'}}/>
                                    </ListItem>
                                    <Divider/>
                                </>
                            )
                        }

                    })}
                    </List>
                </StyledMenu>
            </div>
        </React.Fragment>
    )
}
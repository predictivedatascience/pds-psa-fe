import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { TextField } from "@material-ui/core";
import { LanguageContext } from "../../context/LanguageContext";
import { Localization } from "../../model/localization"


export function PDSDialog(props: any) {
    const localization = React.useContext(LanguageContext)
    return (
        <Dialog
            open={props.open}
            onClose={props.onClose}
            fullWidth={props.fullWidth || false}
            maxWidth={props.maxWidth || 'sm'}
            aria-labelledby="user-dialog-title"
            aria-describedby="user-dialog-description"
        >
            <DialogTitle id="user-dialog-title">{props.title}</DialogTitle>
            <DialogContent style={{ overflowY: "hidden" }}>
                {props.onChange &&
                    <form noValidate autoComplete="off">
                        <TextField id="standard-basic"
                            autoFocus
                            defaultValue={props.value}
                            onChange={(e) => props.onChange(e)}
                        />
                    </form>
                }
                {props.children && props.children}
            </DialogContent>
            <DialogActions>
                <Button onClick={props.onClose} color="default">
                    {props.closeButtonTitle || localization.translations.sap_fi.close}
                </Button>
                {props.onConfirm && <Button onClick={props.onConfirm} color="primary">
                    {props.confirmButtonTitle || localization.translations.common.save}
                </Button>}
            </DialogActions>
        </Dialog>
    );
}
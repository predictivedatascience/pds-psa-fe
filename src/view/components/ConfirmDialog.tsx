import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import { ConfirmDialogProps } from "../../model/props";
import { LanguageContext } from "../../context/LanguageContext";


export default function ConfirmDialog(props: ConfirmDialogProps) {
    const [open, setOpen] = React.useState(props.open);
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('xs'));
    const localization = React.useContext(LanguageContext)


    const handleClose = () => {
        setOpen(false)
        props.onClose()
    };

    return (
        <Dialog
            fullScreen={fullScreen}
            open={open}
            onClose={handleClose}
            aria-labelledby="responsive-dialog-title"
        >
            <DialogTitle id="responsive-dialog-title">{props.title}</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    {props.message}
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button autoFocus onClick={handleClose} color="primary">
                    {props.options && props.options?.second ? props.options.second : `${localization.translations.common.cancel}`}
                </Button>
                <Button onClick={() => { props.onConfirm("aaa"); handleClose() }} color="primary" autoFocus>
                    {props.options && props.options?.first ? props.options.first : `${localization.translations.common.confirm}`}
                </Button>
            </DialogActions>
        </Dialog>
    );
}
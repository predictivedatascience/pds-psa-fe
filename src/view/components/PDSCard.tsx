import React, {Component} from 'react';
import {Box, Button, Card, CardHeader, Grid, Typography} from "@material-ui/core";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import { makeStyles } from "@material-ui/core/styles";
import {  Divider } from "@material-ui/core";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Avatar from "@material-ui/core/Avatar";
import DriveEtaIcon from '@material-ui/icons/DriveEta';
import palette from '../../theme/palette'

interface PDSCardProps {
    id: any,
    title: string,
    cardContent: any,
    cardActions: any
}

class PDSCard extends Component<any, PDSCardProps> {

    render() {
        const {title, id} = this.props
        return (
            <Grid key={id} item style={{width: this.props.width ? this.props.width : "200px"}} >
                    <Card className="spaceHover">
                        <CardHeader
                            title={<Typography variant="subtitle1" style={{verticalAlign: 'middle', display: 'inline-flex'}}>
                                {title}
                            </Typography>
                            }
                            titleTypographyProps={{variant: 'body1'}}
                            style={{background: '#D3DAE6', padding: '0.25rem', textAlign:'center'}}/>

                            {this.props.cardContent &&
                                <CardContent style={{textAlign: "center"}}>
                                    {this.props.cardContent}
                                </CardContent>
                            }

                        {this.props.cardActions &&
                            <CardActions>
                                {this.props.cardActions}
                            </CardActions>
                        }
                    </Card>
            </Grid>
        );
    }
}

export default PDSCard;




const useStyles = makeStyles(theme => ({
    root: {
        //maxWidth: 235,
        position: "relative",
        whiteSpace: 'nowrap',
    },
    cardHeader: {
        backgroundColor: palette.VWblue,
        position: "relative"
    },
    title: {
        marginTop: "30px",
        color: "white",
        display: "flex",
        justifyContent: "center",
        fontSize: "2rem",
},
    textFieldSection: {
        padding: "40px 40px 0 40px"
    },
    settings: {
        padding: 0
    },
    form: {
        position: "relative"
    },
    editButton: {
        position: "absolute",
        left: "20px",
        borderRadius: "50%",
        minWidth: "35px",
        height: "35px",
        padding: "0px",
        paddingLeft: "11px",
        top: "-25px"
    }
}));

export function SpaceCard(props: any) {
    const classes = useStyles();
    return (
        <div>
            <div className={classes.root}>
                <Card /*variant={"outlined"}*/ elevation={0} className="spaceHover">
                    <form>
                        <CardHeader
                            id={"wave"}
                            classes={{title: classes.title}}
                            className={classes.cardHeader}
                        />{/*<span className={'fontColorVW'}>{props.title.charAt(0)}</span>*/}
                        <CardContent className={classes.root}>
                            <Button
                                children={''}
                                variant="contained"
                                color="inherit"
                                className={classes.editButton}
                                startIcon={
                                    //props.icon
                                    <img alt={props.title} src={props.icon} />
                                }
                            />
                            <List>
                                <ListItem>
                                    <ListItemText
                                        primary={<Typography variant="h6" gutterBottom>{props.title}</Typography>}
                                    />
                                </ListItem>
                            </List>
                        </CardContent>
                        <Divider />
                    </form>
                </Card>
            </div>
        </div>
    );
}

import React from 'react';
import 'typeface-roboto';
import { withStyles } from '@material-ui/core/styles';
import Menu, { MenuProps } from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemText from '@material-ui/core/ListItemText';
import SaveAlt from '@material-ui/icons/SaveAlt';
import { IconButton } from "@material-ui/core";
import { LanguageContext } from "../../context/LanguageContext";

const StyledMenu = withStyles({
})((props: MenuProps) => (
    <Menu
        elevation={0}
        getContentAnchorEl={null}
        anchorOrigin={{
            vertical: 'top',
            horizontal: 'center',
        }}
        transformOrigin={{
            vertical: 'top',
            horizontal: 'center',
        }}
        {...props}
    />
));

const StyledMenuItem = withStyles(theme => ({
    root: {
        '&:focus': {
            backgroundColor: "#FFFFFF",
            '& .MuiListItemIcon-root, & .MuiListItemText-primary': {
                color: theme.palette.common.black,
            },
        },
        '&:hover': {
            backgroundColor: "#F5f5f5",
            '& .MuiListItemIcon-root, & .MuiListItemText-primary': {
                color: theme.palette.common.black,
            },
        }
    },
}))(MenuItem);


interface SelectModelButton {
    csvData: string[][]
    fileName: string
}

export default function ExportCsvButton(props: SelectModelButton) {

    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
    const localization = React.useContext(LanguageContext);


    const handleClick = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const exportCsv = (data: string[][], filename: string) => {
        if (filename.indexOf(".csv") === -1) {
            filename += ".csv"
        }
        let csvContent = "data:text/csv;charset=utf-8,"
            + data.map(e => e.join(",")).join("\n");

        let encodedUri = encodeURI(csvContent);
        let link = document.createElement("a");
        link.setAttribute("href", encodedUri);
        link.setAttribute("download", filename);
        document.body.appendChild(link); // Required for FF
        link.click();
        document.body.removeChild(link);
    };

    return (
        <div>
            <React.Fragment>
                <div>
                    <IconButton
                        aria-controls="customized-menu"
                        aria-haspopup="true"
                        color="inherit"
                        onClick={handleClick}
                        style={{ padding: '12px', textAlign: "center" }}
                    >
                        <SaveAlt color={"action"} />
                    </IconButton>
                    <StyledMenu
                        id="customized-menu"
                        anchorEl={anchorEl}
                        keepMounted
                        open={Boolean(anchorEl)}
                        onClose={handleClose}

                    >
                        <StyledMenuItem style={{ padding: "4px 15px", textAlign: "center" }} onClick={() => { exportCsv(props.csvData, props.fileName); handleClose() }} >
                            <ListItemText primary={localization.translations.common.export_csv} />
                        </StyledMenuItem>
                    </StyledMenu>
                </div>
            </React.Fragment>

        </div>
    );
}
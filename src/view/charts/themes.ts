import {Theme} from "@nivo/core";

export const dlqBarChartTheme = {
    axes: {
        axisTitleStyle: {
            fill: '#333',
            fontSize: 15,
            fontStyle: 'normal',
            fontFamily: 'Open Sans, Helvetica, Arial, sans-serif',
            padding: 5,
        },
        tickLabelStyle: {
            fill: '#888888',
            fontSize: 14,
            fontStyle: 'normal',
            fontFamily: 'Open Sans, Helvetica, Arial, sans-serif',
            padding: 5,
        }
    },
    chartPaddings: {
        left: 0,
        right: 0,
        top: 0,
        bottom: 0
    },
    chartMargins: {
        left: 10,
        right: 10,
        top: 10,
        bottom: 10
    },
    lineSeriesStyle: {
        line: {
            visible: true,
            strokeWidth: 1,
            opacity: 1,
        },
        point: {
            visible: true,
            strokeWidth: 1
        },
    },
    barSeriesStyle: {
        displayValue: {
            fontSize: 14,
            fontFamily: 'Open Sans, Helvetica, Arial, sans-serif',
            fontStyle: 'normal',
            fill: "#006BB4",
            padding: 0,
            offsetX: 0,
            offsetY: 20
        }
    },
    colors: {
        defaultVizColor: "#006BB4",
        vizColors: ['#006BB4', 'red', 'orange', 'green', 'purple', 'cyan', 'gray', 'violet' ]

    }
};

export const pdsPieChartTheme: Theme = {
    background: 'transparent',
    legends: {
        text: {
            fontSize: 14
        }
    },
    labels: {
        text: {
            fontSize: 14
        }
    }
};
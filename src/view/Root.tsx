import * as React from "react";
import { ThemeProvider } from "@material-ui/core";
import PDSSingIn from "./PDSSingIn";
import { AuthenticationController } from "../controller/AuthenticationController";
import { DataFetcher } from "../controller/dataFetcher/DataFetcher";
import { BounceLoader } from "react-spinners";
import { Configuration } from "../model/model";
import { theme } from '../theme'
import App from "./App";
import { LanguageContext } from "../context/LanguageContext";
import DE from "../translations/de/translation.json"
import EN from "../translations/en/translation.json"
import { LanguageContexValue } from "../model/localization";


const pickLanguage: () => "EN" | "DE" = () => (
    localStorage.getItem("language") ?
        formatLanguage(localStorage.getItem("language") + "_stored") :
        navigator.language ?
            formatLanguage(navigator.language) :
            "EN"
)

const formatLanguage = (rawLanguage: string | null) => {
    console.log(localStorage.getItem('language'))
    console.log(rawLanguage);
    return rawLanguage && rawLanguage.toLowerCase().indexOf("de") !== -1 ?
        "DE" : "EN"
}
const materialTable = {
    body: {
        emptyDataSourceMessage: 'Keine Einträge',
        addTooltip: 'Hinzufügen',
        deleteTooltip: 'Löschen',
        editTooltip: 'Bearbeiten',
        filterRow: {
            filterTooltip: 'Filter'
        },
        editRow: {
            deleteText: 'Diese Zeile wirklich löschen?',
            cancelTooltip: 'Abbrechen',
            saveTooltip: 'Speichern'
        }
    },
    grouping: {
        placeholder: 'Spalten ziehen ...',
        groupedBy: 'Gruppiert nach:'
    },
    header: {
        actions: 'Aktionen'
    },
    pagination: {
        labelDisplayedRows: '{from}-{to} von {count}',
        labelRowsSelect: 'Zeilen',
        labelRowsPerPage: 'Zeilen pro Seite:',
        firstAriaLabel: 'Erste Seite',
        firstTooltip: 'Erste Seite',
        previousAriaLabel: 'Vorherige Seite',
        previousTooltip: 'Vorherige Seite',
        nextAriaLabel: 'Nächste Seite',
        nextTooltip: 'Nächste Seite',
        lastAriaLabel: 'Letzte Seite',
        lastTooltip: 'Letzte Seite'
    },
    toolbar: {
        addRemoveColumns: 'Spalten hinzufügen oder löschen',
        nRowsSelected: '{0} Zeile(n) ausgewählt',
        showColumnsTitle: 'Zeige Spalten',
        showColumnsAriaLabel: 'Zeige Spalten',
        exportTitle: 'Export',
        exportAriaLabel: 'Export',
        exportName: 'Export als CSV',
        searchTooltip: 'Suche',
        searchPlaceholder: 'Suche'
    }
};

interface IRootState {
    isLoading: boolean;
    loginValid: boolean;
    language: "EN" | "DE"
}

export class Root extends React.Component<{}, IRootState> {

    private authController: AuthenticationController;
    private config: Configuration;
    private dataFetcher: DataFetcher;

    constructor(props: any) {
        super(props);

        this.config = {
            backendURL: process.env.REACT_APP_BACKEND_URL as string
        };
        this.dataFetcher = new DataFetcher(this.config);
        this.authController = new AuthenticationController(this.dataFetcher);

        this.state = {
            isLoading: true,
            loginValid: false,
            language: pickLanguage()
        };
    }
    handleLanguageChange = async (language: "EN" | "DE") => {
        localStorage.setItem('language', language)
        await this.setState({ language: language })
    }
    public async componentDidMount() {
        let isUserLoggedIn: boolean = await this.authController.isUserLoggedIn();
        this.setState({
            loginValid: isUserLoggedIn,
            isLoading: false
        });
    }

    private returnProgressBar() {
        return (
            <div className="fullScreen">
                <BounceLoader
                    css={"margin: -75px 0 0 -75px; position: absolute; top: 50%; left: 50%;"}
                    sizeUnit={"px"}
                    size={150}
                    color={theme.palette.primary.main}
                    loading={true}
                />
            </div>);
    }

    render() {
        console.log(JSON.stringify({ materialTable: materialTable }))
        const languageContextValue = {
            language: this.state.language,
            handleLanguageChange: this.handleLanguageChange,
            translations: this.state.language === "EN" ? EN : DE
        }
        console.log(this.state.language)
        return (
            <React.Fragment>
                <ThemeProvider theme={theme}>
                    <LanguageContext.Provider
                        // @ts-ignore
                        value={languageContextValue}>
                        {
                            this.state.isLoading && (
                                this.returnProgressBar()
                            )
                        }
                        {
                            !this.state.isLoading && !this.state.loginValid && (
                                <PDSSingIn config={this.config} dataFetcher={this.dataFetcher} />
                            )
                        }

                        {!this.state.isLoading && this.state.loginValid &&
                            <App config={this.config} dataFetcher={this.dataFetcher} />
                        }
                    </LanguageContext.Provider>
                </ThemeProvider>
            </React.Fragment>)
    }

}
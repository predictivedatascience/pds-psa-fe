import * as React from "react"
import { useState } from "react"
// @ts-ignore
import { createStyles, Theme } from "@material-ui/core"
import { DataFetcher } from "../controller/dataFetcher/DataFetcher"
import { Configuration } from "../model/model"
import { flowChartContext } from "../context/FlowChartContect"
import { makeStyles } from "@material-ui/core/styles"
import { BrowserRouter } from "react-router-dom"
import routes from "../router/routes"
import CustomRouter from "../router/CustomRouter"
import SelectLanguageButton from "./components/SelectLanguageButton"
import { LanguageContext } from "../context/LanguageContext"
import { Localization } from "../model/localization"
import moment from "moment"

interface IAppState {
  drawerOpen: boolean
  line: string
  modelKeyword: string
}

interface IAppStateProps {
  config: Configuration
  dataFetcher: DataFetcher
  line?: string
  modelKeyword?: string
  date?: string
  handleDateChange?: any
  handleHalleUpdate?: any
  handleHalleChange?: any
  handleModelChange?: any
}

let pdfIncompatibleDomains = ["/umlaufverweildauer"]

const drawerWidth = 240
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
    },
    container: {
      /*display: 'flex',
            flex: '1 1 auto',*/
      overflow: "hidden",
    },
    small: {
      width: theme.spacing(4),
      height: theme.spacing(4),
      textTransform: "uppercase",
      backgroundColor: theme.palette.primary.main,
      color: theme.palette.text.secondary,
    },
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
      transition: theme.transitions.create(["width", "margin"], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
    },
    appBarShift: {
      marginLeft: drawerWidth,
      width: `calc(100% - ${drawerWidth}px)`,
      transition: theme.transitions.create(["width", "margin"], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    menuButton: {
      marginRight: 36,
    },
    hide: {
      display: "none",
    },
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
      whiteSpace: "nowrap",
    },
    drawerOpen: {
      width: drawerWidth,
      transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
      overflowX: "hidden",
    },
    drawerClose: {
      transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      overflowX: "hidden",
      width: theme.spacing(7) + 1,
      [theme.breakpoints.up("sm")]: {
        width: theme.spacing(9) + 1,
      },
    },
    toolbar: {
      display: "flex",
      alignItems: "center",
      justifyContent: "flex-end",
      padding: theme.spacing(0, 1),
      ...theme.mixins.toolbar,
    },
    toolbarItem: {
      display: "flex",
      alignItems: "center",
      justifySelf: "flex-start",
      padding: theme.spacing(0, 1),
      ...theme.mixins.toolbar,
    },
    bottomPush: {
      position: "fixed",
      bottom: 0,
      textAlign: "center",
      paddingBottom: 10,
      ...theme.mixins.toolbar,
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(2),
    },
  })
)

const App = (props: IAppStateProps) => {
  const classes = useStyles()
  const [line, setLine] = useState("2")
  const [modelKeyword, setModelKeyword] = useState("E11")
  const [date, setDate] = useState(moment().format("YYYY-MM-DD"))
  const localization = React.useContext(LanguageContext)

  const handleHalleChange = (event: any) => {
    setLine(event.target.value)
  }
  const handleHalleUpdate = (halle: string) => {
    setLine(halle)
  }
  const handleModelChange = (modelKeyword: string) => {
    setModelKeyword(modelKeyword)
  }
  const handleDateChange = async (date: Date | any) => {
    let newDate = moment(date).format("YYYY-MM-DD")
    let currentDate = moment().format("YYYY-MM-DD")

    if (currentDate > newDate) {
      setDate(newDate)
    } else {
      setDate(currentDate)
    }
  }
  return (
    <flowChartContext.Provider
      value={{
        line: line,
        modelKeyword: modelKeyword,
        handleChangeHalle: handleHalleChange,
        handleModelChange: handleModelChange,
      }}
    >
      <div className={classes.root}>
        <div className={classes.content}>
          <BrowserRouter>
            <CustomRouter
              line={line}
              modelKeyword={modelKeyword}
              date={date}
              routes={routes}
              config={props.config}
              dataFetcher={props.dataFetcher}
              handleHalleUpdate={handleHalleUpdate}
              handleDateChange={handleDateChange}
              handleModelChange={handleModelChange}
              handleHalleChange={handleHalleChange}
            />
          </BrowserRouter>
        </div>
      </div>
    </flowChartContext.Provider>
  )
}

export default App

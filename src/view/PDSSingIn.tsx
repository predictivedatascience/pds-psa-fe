import React, {SyntheticEvent} from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { AuthenticationController } from "../controller/AuthenticationController";
import { DataFetcher } from "../controller/dataFetcher/DataFetcher";
import { theme } from '../theme'
import App from "./App";
import { MySnackbarContentWrapper } from "./components/PDSSnackbar";
import { Configuration } from "../model/model";
import { IconButton, InputAdornment } from "@material-ui/core";
import { Visibility, VisibilityOff } from "@material-ui/icons";
import { LanguageContext } from '../context/LanguageContext';
import SelectLanguageButton from "./components/SelectLanguageButton";


interface IPDSSingInState {
    waitingForLoginResponse: boolean;
    proceedToApp: boolean;
    errorMsg?: string;
    errorMsgShown?: boolean;
    showPassword: boolean;
}

interface IPDSSignInProps {
    config: Configuration;
    dataFetcher: DataFetcher;
}

export default class PDSSingIn extends React.Component<IPDSSignInProps, IPDSSingInState> {

    private authController: AuthenticationController;
    private bgImageStyles: any;
    private paperStyles: any;
    private headStyles: any;
    private avatarStyles: any;
    private formStyles: any;
    private submitStyles: any;
    private logo: any;
    private copyright: any;
    private lang: any;

    constructor(props: any) {
        super(props);

        this.authController = new AuthenticationController(this.props.dataFetcher);

        this.initStyles();

        this.handleSubmittedForm = this.handleSubmittedForm.bind(this);
        this.handleCloseSnackbar = this.handleCloseSnackbar.bind(this);

        this.state = {
            waitingForLoginResponse: false,
            proceedToApp: false,
            showPassword: false
        };
    }

    private initStyles() {
        this.bgImageStyles = {
            backgroundImage: 'url(/img/robot_big_res.jpg)',
            backgroundRepeat: 'no-repeat',
            backgroundColor: theme.palette.grey[50],
            backgroundSize: 'cover',
            backgroundPosition: 'center',
        };

        this.paperStyles = {
            height: '95vh',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center'
        };
        this.headStyles = {
            height: '5vh',
            marginRight: '5px',
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'flex-end',
            justifyContent: 'flex-end'
        }

        this.avatarStyles = {
            margin: theme.spacing(1),
            backgroundColor: theme.palette.secondary.main,
        };

        this.formStyles = {
            width: '100%', // Fix IE 11 issue.
            marginTop: theme.spacing(1),
        };
        this.logo = {
            marginBottom: '10vh',
        };

        this.lang = {
            alignSelf: 'flex-end',
        };

        this.copyright = {
            marginTop: '15vh',
        };

        this.submitStyles = {
            margin: theme.spacing(3, 0, 2),
            //backgroundColor: '#00315B'
        };
    }

    private async tryLogin(username: string, pwd: string) {

        this.setState({waitingForLoginResponse: true, proceedToApp: false});

        this.authController.tryLogin(username, pwd).then(
            (onFulfilled) => {
                this.setState({waitingForLoginResponse: false, proceedToApp: true});
            },
            (onRejected) => {
                this.setState({
                    waitingForLoginResponse: false,
                    proceedToApp: false,
                    errorMsgShown: true,
                    errorMsg: onRejected.toString()
                    //errorMsg: 'You have entered an invalid username or password'
                });
            });
    }


    private handleSubmittedForm(event: any) {
        event.preventDefault();

        let object: any = {};

        const data: FormData = new FormData(event.target);

        data.forEach((value, key) => {
            object[key] = value
        });
        this.tryLogin(object.username, object.password);
    }

    private handleCloseSnackbar(event?: SyntheticEvent, reason?: string) {
        if (reason === 'clickaway') {
            return;
        }
        this.setState({errorMsgShown: false});
    }
    static contextType = LanguageContext;
    render() {
        const translations = this.context.translations
        if (this.state.proceedToApp) {
            return <App config={this.props.config} dataFetcher={this.props.dataFetcher}/>;
        }
        const handleClickShowPassword = () => this.setState({showPassword: !this.state.showPassword});
        const handleMouseDownPassword = () => this.setState({showPassword: !this.state.showPassword});

        return (
            <Grid container component="main" style={{height: '100vh'}}>
                {
                    this.state.errorMsg && (
                        <MySnackbarContentWrapper
                            variant="error"
                            onClose={this.handleCloseSnackbar}
                            open={!!this.state.errorMsgShown}
                            message={this.state.errorMsg}
                        />
                    )
                }
                <CssBaseline/>
                <Grid item xs={false} sm={4} md={7} style={this.bgImageStyles}/>
                <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
                    <Box style={this.headStyles}>
                        <Box style={this.lang}>
                            <SelectLanguageButton />
                        </Box>
                    </Box>
                    <Box style={this.paperStyles}>
                        <Box style={this.logo}>
                            <img src="/img/pds_logo_web.png"
                                 alt="PDS Logo" height="50"/>
                        </Box>
                        <Box maxWidth="80%">
                            <form style={this.formStyles} onSubmit={this.handleSubmittedForm}>
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    required
                                    fullWidth
                                    id="username"
                                    label={translations.user.username}
                                    name="username"
                                    autoFocus
                                />
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    required
                                    fullWidth
                                    name="password"
                                    label={translations.user.password}
                                    type={this.state.showPassword ? "text" : "password"}
                                    id="password"
                                    autoComplete="current-password"
                                    InputProps={{
                                        endAdornment: (
                                            <InputAdornment position="end">
                                                <IconButton
                                                    aria-label="toggle password visibility"
                                                    onClick={handleClickShowPassword}
                                                    onMouseDown={handleMouseDownPassword}
                                                >
                                                    {this.state.showPassword ? <Visibility /> : <VisibilityOff />}
                                                </IconButton>
                                            </InputAdornment>
                                        )
                                    }}
                                />
                                <Button
                                    id="btn_login"
                                    type="submit"
                                    fullWidth
                                    variant="contained"
                                    color="primary"
                                    className={'login-button'}
                                    disabled={this.state.waitingForLoginResponse}
                                    style={this.submitStyles}
                                >
                                    {translations.user.log_in}
                                </Button>
                                <GetBrowser/>
                            </form>
                        </Box>
                        <Box style={this.copyright}  alignItems="flex-end">
                            <Copyright/>
                        </Box>

                    </Box>
                </Grid>
            </Grid>
        );
    }
}

class GetBrowser extends React.Component{
    render(){
        let getIE = navigator.userAgent.search(/(?:Edge|MSIE|Trident\/.*; rv:)/)
        if (getIE !== -1){
            return(
                <div style={{"backgroundColor": 'rgb(253, 236, 234)', 'color': 'rgb(97, 26, 21)',  'padding': '15px'}}>
                    <h6>Warning</h6>
                    Internet Explorer is not supported!
                </div>
            )
        }
        else return null
    }
}

class Copyright extends React.Component {

    render() {
        return (
            <Grid >

                <div style={{alignItems: 'center',textAlign: 'center', margin: '5px auto'}}>
                    <Link  href="https://predictivedatascience.sk">
                        <img
                            src="/img/pdsLogo.svg"
                            alt="PDS Logo" height="25" width="190"/>
                    </Link>
                </div>
                <Typography variant="body2" color="textSecondary" align="center">
                    {'Copyright © '}
                    <Link color="inherit" href="https://predictivedatascience.sk">
                        PredictiveDataScience s.r.o.
                    </Link>{' '}
                    {new Date().getFullYear()}
                </Typography>
            </Grid>
        );
    }
}
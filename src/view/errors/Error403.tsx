import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import { Typography, Button, useTheme, useMediaQuery } from '@material-ui/core';


const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(3),
        paddingTop: '25vh',
        display: 'flex',
        flexDirection: 'column',
        alignContent: 'center',
        backgroundImage:"url(/img/wallpaper-vw.jpg)",
        backgroundSize:"cover", backgroundRepeat:"no-repeat",
        width: '100vw', height: '100vh',
        position:"fixed", left:0, top:0,
    },
    imageContainer: {
        marginTop: theme.spacing(6),
        display: 'flex',
        justifyContent: 'center'
    },
    image: {
        maxWidth: '100%',
        width: 560,
        maxHeight: 300,
        height: 'auto'
    },
    buttonContainer: {
        marginTop: theme.spacing(6),
        display: 'flex',
        justifyContent: 'center'
    }
}));

const Error403 = () => {
    const classes = useStyles();
    const theme = useTheme();
    const mobileDevice = useMediaQuery(theme.breakpoints.down('sm'));

    return (
        <div
            className={classes.root}
        >
            <Typography
                align="center"
                variant={mobileDevice ? 'h4' : 'h1'}
            >
                403: No Permission
            </Typography>
            <Typography
                align="center"
                variant="subtitle2"
            >
                Sorry, but you don't have permission to access this page
            </Typography>
            <div className={classes.imageContainer}>
                <img
                    alt="Under development"
                    className={classes.image}
                    src="/img/undraw_server_down_s4lk.svg"
                />
            </div>
            <div className={classes.buttonContainer}>
                <Button
                    color="primary"
                    component={RouterLink}
                    to="/spaces"
                    variant="outlined"
                >
                    Back to home
                </Button>
            </div>

        </div>
    );
};
export default Error403;

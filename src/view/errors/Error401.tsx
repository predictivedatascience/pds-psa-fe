import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import { Typography, Button, useTheme, useMediaQuery } from '@material-ui/core';


const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(3),
        paddingTop: '25vh',
        display: 'flex',
        flexDirection: 'column',
        alignContent: 'center',
        backgroundImage:"url(/img/wallpaper-vw.jpg)",
        backgroundSize:"cover", backgroundRepeat:"no-repeat",
        width: '100vw', height: '100vh',
        position:"fixed", left:0, top:0,
    },
    imageContainer: {
        marginTop: theme.spacing(6),
        display: 'flex',
        justifyContent: 'center'
    },
    image: {
        maxWidth: '100%',
        width: 560,
        maxHeight: 300,
        height: 'auto'
    },
    buttonContainer: {
        marginTop: theme.spacing(6),
        display: 'flex',
        justifyContent: 'center'
    }
}));

const Error403 = () => {
    const classes = useStyles();
    const theme = useTheme();
    const mobileDevice = useMediaQuery(theme.breakpoints.down('sm'));

    return (
        <div
            className={classes.root}
        >
            <Typography
                align="center"
                variant={mobileDevice ? 'h4' : 'h1'}
            >
                401: We are sorry but we are not able to authenticate you.
            </Typography>
            <Typography
                align="center"
                variant="subtitle2"
            >
                You either tried some shady route or you came here by mistake. Whichever
                it is, try using the navigation
            </Typography>
            <div className={classes.imageContainer}>
                <img
                    alt="Under development"
                    className={classes.image}
                    src="/img/undraw_authentication_fsn5.svg"
                />
            </div>
            <div className={classes.buttonContainer}>
                <Button
                    color="primary"
                    component={RouterLink}
                    to="/auth/login"
                    variant="outlined"
                >
                    Back to login
                </Button>
            </div>
        </div>
    );
};

export default Error403;

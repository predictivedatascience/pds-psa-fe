import { createContext } from 'react';
import DE from "../translations/de/translation.json"
import EN from "../translations/en/translation.json"
import {LanguageContexValue, Localization} from "../model/localization";


const defaultContext:LanguageContexValue = {language:"EN", translations: {} as Localization, handleLanguageChange: (language:"EN"|"DE")=>{}}

export const LanguageContext = createContext(defaultContext)
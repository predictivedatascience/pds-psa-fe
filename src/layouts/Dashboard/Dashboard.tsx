import React, { useRef, useState } from 'react';
import { makeStyles } from '@material-ui/styles';
import { NavBar, TopBar } from './components';
import { Theme, createStyles } from "@material-ui/core";
import Container from "./components/Container";
import '@elastic/charts/dist/theme_light.css';
import clsx from "clsx";

const drawerWidth = 240;
const useStyles = makeStyles((theme: Theme) => createStyles({
    root: {
        paddingTop: '50px',
        display: 'flex',
        flexDirection: 'column',
    },
    container: {
        /*display: 'flex',
         flex: '1 1 auto',*/
        overflow: 'hidden'
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: 450,
        }),
    },
    appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: 400,
        }),
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(1),
        margin: '0 auto',
        marginLeft: '6vw',
        marginRight: '.6vw',
        //maxWidth: 'calc(90vw + 71px)'
        maxWidth: '90vw'
    },
    spacesContent: {
        flexGrow: 1,
        padding: theme.spacing(1),
        margin: '0 auto'
    },
}));

const Dashboard = (props: any) => {
    const classes = useStyles();
    const [openNavBarMobile, setOpenNavBarMobile] = useState(false);
    const { openMobile, onMobileClose, className, ...rest } = props;
    const [drawerOpen, setDrawerOpen] = useState(false)
    const notificationsRef = useRef(null);
    const [openNotifications, setOpenNotifications] = useState(false);


    const handleNotificationsOpen = () => {
        setOpenNotifications(true);
    };

    const handleNotificationsClose = () => {
        setOpenNotifications(false);
    };

    const toggleDrawer = (open: boolean) => (event: React.KeyboardEvent | React.MouseEvent) => {
        if (event.type === 'keydown' && ((event as React.KeyboardEvent).key === 'Tab' ||
            (event as React.KeyboardEvent).key === 'Shift')) {
            return;
        }
        setDrawerOpen(open)
    };

    const handleNavBarMobileOpen = () => {
        setOpenNavBarMobile(true);
    };

    const handleNavBarMobileClose = () => {
        setOpenNavBarMobile(false);
    };
    let getRouteParams = props.routes.filter((i: any) => i.path.replace(/^\/([^\/]*).*$/, '$1') === props.pathname.replace(/^\/([^\/]*).*$/, '$1'));

    return (
        <div className={classes.root}>
            <TopBar
                {...props}
                drawerOpen={drawerOpen}
                className={classes.appBar}
                notificationsRef={notificationsRef}
                onOpenNavBarMobile={handleNavBarMobileOpen}
                openNotifications={openNotifications}
                handleNotificationsOpen={handleNotificationsOpen}
                handleNotificationsClose={handleNotificationsClose}
                dataFetcher={props.dataFetcher}
                line={props.line}
                modelKeyword={props.modelKeyword}
                pdfExport={getRouteParams[0] && getRouteParams[0].pdfExport}
            />
            <div className={classes.container}>
                {props.pathname !== '/spaces' && props.pathname !== '/' &&
                    <NavBar
                        {...props}
                        drawerOpen={drawerOpen}
                        toggleDrawer={toggleDrawer}
                        onMobileClose={handleNavBarMobileClose}
                        openMobile={openNavBarMobile}
                    />
                }
                <main className={clsx(classes.content, { [classes.spacesContent]: (props.pathname === '/spaces' && props.pathname !== '/') })}
                    id="DashboardContainer"
                >
                    <Container {...props} />
                </main>
            </div>
        </div>
    );
};

export default Dashboard;

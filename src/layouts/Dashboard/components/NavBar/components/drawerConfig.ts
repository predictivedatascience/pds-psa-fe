import FeaturedPlayListIcon from "@material-ui/icons/FeaturedPlayList";
import TimelineIcon from '@material-ui/icons/Timeline';
import FlashAutoIcon from '@material-ui/icons/FlashAuto';
import PublicIcon from '@material-ui/icons/Public';
import EventAvailableIcon from '@material-ui/icons/EventAvailable';
import LayersIcon from '@material-ui/icons/Layers';
import Filter1Icon from '@material-ui/icons/Filter1';

export default {
    qsmr_dashboard: {
        title: 'QSMR',
        children: [{
            title: 'Select Space',
            href: '/spaces',
            icon: LayersIcon,
            roles: ['ADMIN', 'QSMR']
        }]
    },
    qsmr_id_dashboard: {
        title: 'QS ID Reports',
        children: [{
            title: 'Select Space',
            href: '/spaces',
            icon: LayersIcon,
            roles: ['ADMIN','QSMR_ID', 'QSMR']
            },
            {
                title: '1st level',
                href: '/qsmr_id_dashboard',
                icon: Filter1Icon,
                roles: ['ADMIN','QSMR_ID', 'QSMR']
            }]
    },
    qsaudit_dashboard: {
        title: 'QS Audit Reports',
        children: [{
            title: 'Select Space',
            href: '/spaces',
            icon: LayersIcon,
            roles: ['ADMIN', 'QSAUDIT']
            },
            {
                title: '1st level',
                href: '/qsaudit_dashboard',
                icon: Filter1Icon,
                roles: ['ADMIN', 'QSAUDIT']
            }]
    },
    qspr_dashboard: {
        title: "Produktion",
        // icon: DashboardIcon,
        children: [
            {
                title: 'Select Space',
                href: '/spaces',
                icon: LayersIcon,
                roles: ['ADMIN', 'QSPR']
            },
            {
                title: 'Flussdiagram',
                href: '/qspr_dashboard/QSProduction/model/E11',
                icon: TimelineIcon,
                roles: ['QSPR', 'ADMIN']
            },
            {
                title: 'Umlauftabelle',
                href: '/qspr_dashboard/QSProduction/umlauftabelle',
                icon: FeaturedPlayListIcon,
                roles: ['ADMIN', 'QSPR']
            },
            {
                title: 'Summary Table',
                href: '/summary_dashboard',
                icon: EventAvailableIcon,
                roles: ['ADMIN', 'ATLAS']
            },
            {
                title: 'Atlas',
                href: '/atlas_dashboard',
                 icon: PublicIcon,
                roles: ['ADMIN', 'ATLAS']
            },
            {
                title: 'Anlaufbericht',
                href: '/flashaktion',
                 icon: FlashAutoIcon,
                roles: ['ADMIN', 'ATLAS', 'QSPR']
            },
        ]
    },
    atlas_dashboard: {
        title: "Atlas",
        children: [{
                title: 'Select Space',
                href: '/spaces',
                icon: LayersIcon,
                roles: ['ADMIN', 'ATLAS', 'QSPR']
            },
            {
                title: 'Umlauftabelle',
                href: '/qspr_dashboard/QSProduction/umlauftabelle',
                icon: FeaturedPlayListIcon,
                roles: ['ADMIN', 'QSPR']
            },
            {
                title: 'Summary Table',
                href: '/summary_dashboard',
                icon: EventAvailableIcon,
                roles: ['ADMIN', 'ATLAS']
            },
            {
                title: 'Atlas',
                href: '/atlas_dashboard',
                icon: PublicIcon,
                roles: ['ADMIN', 'ATLAS']
            },
            {
                title: 'Anlaufbericht',
                href: '/flashaktion',
                icon: FlashAutoIcon,
                roles: ['ADMIN', 'ATLAS', 'QSPR']
            },
        ]
    },
    schleppeliste: {
        title: 'Schleppeliste',
        children: [{
            title: 'Select Space',
            href: '/spaces',
            icon: LayersIcon,
            roles: ['ADMIN', 'ATLAS']
        }]
    },
    umlaufverweildauer: {
        title: 'Umlaufverweildauer',
        children: [{
            title: 'Select Space',
            href: '/spaces',
            icon: LayersIcon,
            roles: ['ADMIN', 'UMLAUFVERWEILDAUER', 'ATLAS']
        }]
    },
    umlauffehlertabelle: {
        title: 'Offene Fehler im Umlauf',
        children: [{
            title: 'Select Space',
            href: '/spaces',
            icon: LayersIcon,
            roles: ['ADMIN', 'ATLAS', 'QSPR', 'OFU']
        }]
    },
    gesamtfehlertabelle: {
        title: 'Gesamtfehler',
        children: [{
            title: 'Select Space',
            href: '/spaces',
            icon: LayersIcon,
            roles: ['ADMIN', 'ATLAS', 'QSPR', 'GESAMT']
        }]
    },
    rtp_dashboard: {
        title: 'Road Test Prediction',
        children: [{
            title: 'Select Space',
            href: '/spaces',
            icon: LayersIcon,
            roles: ['ADMIN', 'RTP']
        }]
    },
    schraubdaten: {
        title: 'Schraubdaten',
        children: [{
            title: 'Select Space',
            href: '/spaces',
            icon: LayersIcon,
            roles: ['ADMIN', 'SCHRAUBDATEN']
        }]
    },
    dlq_be: {
        title: 'DLQ BE',
        children: [{
            title: 'Select Space',
            href: '/spaces',
            icon: LayersIcon,
            roles: ['ADMIN', 'ATLAS']
        }]
    },
    atlas_platin: {
        title: 'atlas_platin',
        children: [{
            title: 'Select Space',
            href: '/spaces',
            icon: LayersIcon,
            roles: ['ADMIN', 'ATLAS']
        }]
    },
    summary_dashboard: {
        title: 'Summary dashboard',
        children: [{
            title: 'Select Space',
            href: '/spaces',
            icon: LayersIcon,
            roles: ['ADMIN', 'ATLAS']
            },
            {
                title: 'Umlauftabelle',
                href: '/qspr_dashboard/QSProduction/umlauftabelle',
                icon: FeaturedPlayListIcon,
                roles: ['ADMIN', 'QSPR']
            },
            {
                title: 'Summary Table',
                href: '/summary_dashboard',
                icon: EventAvailableIcon,
                roles: ['ADMIN', 'ATLAS']
            },
            {
                title: 'Atlas',
                href: '/atlas_dashboard',
                icon: PublicIcon,
                roles: ['ADMIN', 'ATLAS']
            },
            {
                title: 'Anlaufbericht',
                href: '/flashaktion',
                icon: FlashAutoIcon,
                roles: ['ADMIN', 'ATLAS', 'QSPR']
            },]
    },
    flashaktion: {
        title: 'Flashaktion',
        children: [{
                title: 'Select Space',
                href: '/spaces',
                icon: LayersIcon,
                roles: ['ADMIN', 'ATLAS', 'QSPR']
            },
            {
                title: 'Atlas',
                href: '/atlas_dashboard',
                icon: PublicIcon,
                roles: ['ADMIN', 'ATLAS']
            },]
    },
    flashaktionID4: {
        title: 'Flashaktion',
        children: [{
                title: 'Select Space',
                href: '/spaces',
                icon: LayersIcon,
            roles: ['ADMIN', 'ATLAS', 'QSPR']
            },
            {
                title: 'Atlas',
                href: '/atlas_dashboard',
                icon: PublicIcon,
                roles: ['ADMIN', 'ATLAS']
            },]
    },
    schichtenvergleich: {
        title: 'Schichtenvergleich',
        children: [{
            title: 'Select Space',
            href: '/spaces',
            icon: LayersIcon,
            roles: ['ADMIN', 'ATLAS']
        }]
    },
    konfigurierbares: {
       title: 'Konfigurierbares Reporting',
        children: [{
            title: 'Select Space',
            href: '/spaces',
            icon: LayersIcon,
            roles: ['ADMIN']
        }]
    },
    add_user: {
        title: 'Settings',
        children: [{
            title: 'Select Space',
            href: '/spaces',
            icon: LayersIcon,
            roles: ['ADMIN']
        }]
    },
    delete_user: {
        title: 'Settings',
        children: [{
            title: 'Select Space',
            href: '/spaces',
            icon: LayersIcon,
            roles: ['ADMIN']
        }]
    },
    account_settings: {
        title: 'Settings',
        children: [{
            title: 'Select Space',
            href: '/spaces',
            icon: LayersIcon,
            roles: ['ADMIN']
        }]
    },
    roles_settings: {
        title: 'Roles',
        children: [{
            title: 'Select Space',
            href: '/spaces',
            icon: LayersIcon,
            roles: ['ADMIN', 'MANAGE']
        }]
    },
    email: {
        title: 'Email',
        children: [{
            title: 'Select Space',
            href: '/spaces',
            icon: LayersIcon,
            roles: ['ADMIN']
        }]
    },
    about: {
        title: 'About',
        children: [{
            title: 'Select Space',
            href: '/spaces',
            icon: LayersIcon,
            roles: ['ADMIN']
        }]
    },
    monitoring: {
        title: 'Monitoring',
        children: [{
            title: 'Select Space',
            href: '/spaces',
            icon: LayersIcon,
            roles: ['ADMIN', 'MONITORING']
        }]
    },
    direktlauf_report: {
        title: 'Direktlauf',
        children: [
            {
                title: 'Select Space',
                href: '/spaces',
                icon: LayersIcon,
                roles: ['ADMIN', 'SBBM']
            },
            {
                title: '1st level',
                href: '/direktlauf_report',
                icon: Filter1Icon,
                roles: ['ADMIN', 'SBBM']
            }]
    },
    wsus: {
        title: 'WSUS',
        children: [
            {
                title: 'Select Space',
                href: '/spaces',
                icon: LayersIcon,
                roles: ['ADMIN', 'WSUS']
            },
            ]
    },
}
import {Divider, List, ListItem, ListItemIcon, ListItemText,} from "@material-ui/core";
import * as React from "react";
import {theme} from '../../../../../theme'
import {getUserRole, isLdapUser} from "../../../../../controller/RoleController";
import {Link as RouterLink} from 'react-router-dom';
import LockIcon from '@material-ui/icons/Lock';
import palette from "../../../../../theme/palette";
import drawerConfig from "./drawerConfig";

interface IDrawerListState {
    accountSettingsExpanded: boolean;
}

export class DrawerList extends React.Component<any, IDrawerListState> {

    private rootStyles: any;
    private nested: any;
    private doubleNested: any;

    constructor(props: any) {
        super(props);

        this.initStyles();

        this.handleClickNavigateToDashboard = this.handleClickNavigateToDashboard.bind(this);
        this.handleAccountSettingsAction = this.handleAccountSettingsAction.bind(this);

        this.state = {
            accountSettingsExpanded: false,
        }
    }

    public get_id(id: string) {
        return id;
    }

    public render() {

        let role = getUserRole();
        let ldapUser = isLdapUser();
        let drawerID = this.props.routeID as keyof typeof drawerConfig;

        let TQO = "none";
        let AS1 = "none";
        let AS2 = "none";

        if (role.includes("ADMIN") || role.includes("MANAGE")) {
            TQO = "flex";
            AS1 = "flex";
            AS2 = "none";
        }

        let changePasswordLDAP = (
            <ListItem style={this.nested} button
                      onClick={() => {
                          this.handleClickNavigateToDashboard('/account_settings')
                      }}>
                <ListItemIcon>
                    <LockIcon style={{color: '#00315B'}}/>
                </ListItemIcon>
                <ListItemText primary="Change password"/>
            </ListItem>
        );

        return <List
            component="nav"
            aria-labelledby="nested-list-subheader"
            style={this.rootStyles}
        >
            {drawerConfig[drawerID] && drawerConfig[drawerID].children.map((i: { href: string, title: string, icon: any, roles: string[] }) => {
                let findRole = i.roles.some((i: any) => role.includes(i))
                if (i.title && i.href && i.icon && findRole) {
                    return (
                        <React.Fragment>
                            <ListItem button component={RouterLink} to={i.href}>
                                <ListItemIcon>
                                    <i.icon style={{color: '#00315B'}}/>
                                </ListItemIcon>
                                <ListItemText primary={i.title}/>
                            </ListItem>
                            <Divider/>
                        </React.Fragment>)
                }
            })}
        </List>
    }

    private initStyles() {
        this.rootStyles = {
            width: '100%',
            maxWidth: 360,
            backgroundColor: theme.palette.background.paper,
            marginLeft: '5px',
            color: palette.VWblue
        };

        this.nested = {
            paddingLeft: theme.spacing(4),
        };

        this.doubleNested = {
            paddingLeft: theme.spacing(8),
        };
    }

    private handleClickNavigateToDashboard(url: string) {
        window.location.href = url;
    }

    private handleAccountSettingsAction() {
        let accountSettingsExp: boolean = this.state.accountSettingsExpanded;
        this.setState({accountSettingsExpanded: !accountSettingsExp});
    }
}
import React, {Fragment, useState} from 'react';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import {
    Drawer,
    Typography,
    IconButton,
    createStyles,
    Theme
} from '@material-ui/core';
import {DrawerList} from "./components/DrawerList";
import FormatIndentDecreaseIcon from "@material-ui/icons/FormatIndentDecrease";
import FormatIndentIncreaseIcon from "@material-ui/icons/FormatIndentIncrease";
import palette from "../../../../theme/palette";

const drawerWidth = 240;
const useStyles = makeStyles((theme: Theme) => createStyles({
    root: {
        height: '100%',
        overflowY: 'auto',
        color: palette.text.primary
    },
    menuButton: {
        marginRight: 36,
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
        whiteSpace: 'nowrap',
    },
    drawerOpen: {
        width: drawerWidth,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            //duration: theme.transitions.duration.enteringScreen,
            duration: 400,
        }),
        overflowX: 'hidden'
    },
    drawerClose: {
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: 450,
        }),
        overflowX: 'hidden',
        width: theme.spacing(7) + 1,
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing(9) + 1,
        },
    },
    toolbar: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-start',
        padding: theme.spacing(0, 1),
        ...theme.mixins.toolbar,
    },
    toolbarItem: {
        display: 'flex',
        alignItems: 'center',
        justifySelf: 'flex-start',
        padding: theme.spacing(0, 1),
        ...theme.mixins.toolbar,
    },
    bottomPush: {
        position: "fixed",
        bottom: 0,
        textAlign: "center",
        paddingBottom: 10,
        ...theme.mixins.toolbar,
    },
}));

const NavBar = (props:any) => {
    const {openMobile, onMobileClose, className, ...rest } = props;
    const classes = useStyles();
    const [drawerOpen, setDrawerOpen] = useState(false)


    const toggleDrawer = (open: boolean) => (event: React.KeyboardEvent | React.MouseEvent) => {
        if (event.type === 'keydown' && ((event as React.KeyboardEvent).key === 'Tab' ||
            (event as React.KeyboardEvent).key === 'Shift')) {
            return;
        }
        setDrawerOpen(open)
    };

    return (
        <Fragment>
            <Drawer
                open={drawerOpen}
                onMouseEnter={props.toggleDrawer(true)}
                onMouseLeave={props.toggleDrawer(false)}
                onClose={props.toggleDrawer(false)}
                variant="permanent"
                className={clsx(classes.drawer, {
                    [classes.drawerOpen]: props.drawerOpen,
                    [classes.drawerClose]: !props.drawerOpen
                })}
                classes={{paper: clsx({
                        [classes.drawerOpen]: props.drawerOpen,
                        [classes.drawerClose]: !props.drawerOpen
                    })
                }}
            >
                <div className={classes.toolbar}>
                    <RouterLink to={"/spaces"}>
                            <img style={{padding: '0', marginLeft: '10px'}} src="/img/pds_logo_short.png" alt="PDS logo" height="25"/>
                    </RouterLink>
                    <RouterLink to={"/spaces"}>

                            <Typography variant="h2" style={{flexGrow: 1, marginLeft: '10px', color: palette.text.primary}}>
                                IDA
                            </Typography>

                    </RouterLink>
                    {/*<IconButton onClick={toggleDrawer(!drawerOpen)}>
                            {theme.direction === 'rtl' ? <FormatIndentDecreaseIcon/> : <FormatIndentDecreaseIcon/>}
                        </IconButton>*/}
                </div>

                <DrawerList {...props} />

                <div className={classes.toolbar}>
                    <IconButton className={classes.bottomPush} onClick={toggleDrawer(!drawerOpen)}>
                        {props.drawerOpen ? <FormatIndentDecreaseIcon /> : <FormatIndentIncreaseIcon />}
                    </IconButton>
                </div>
            </Drawer>
        </Fragment>
    );
};

export default NavBar;

import React, { Fragment, useEffect, useRef, useState } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import {
    Toolbar,
    AppBar,
    createStyles,
    Theme, Divider, Collapse, Chip, colors, Grid
} from '@material-ui/core';
import { SelectSpaceButton } from "../../../../view/components/SelectSpaceButton";
import { AuthenticationController } from "../../../../controller/AuthenticationController";
import {getUserName, getUserRole, isLdapUser } from "../../../../controller/RoleController";
import NotificationsPopover from "../../../../view/components/NotificationsPopover";
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import PowerSettingsNewIcon from '@material-ui/icons/PowerSettingsNew';
import SettingsIcon from "@material-ui/icons/Settings";
import EqualizerIcon from "@material-ui/icons/Equalizer";
import { ExpandLess, ExpandMore } from "@material-ui/icons";
import PersonAddIcon from "@material-ui/icons/PersonAdd";
import PersonAddDisabledIcon from "@material-ui/icons/PersonAddDisabled";
import PeopleIcon from "@material-ui/icons/People";
import EmailIcon from "@material-ui/icons/Email";
import InfoIcon from "@material-ui/icons/Info";
import LockIcon from "@material-ui/icons/Lock";
import palette from "../../../../theme/palette";
import { PDFButton } from "../../../../view/components/PDFButton/PDFButton";
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import SelectLanguageButton from '../../../../view/components/SelectLanguageButton';
import { LanguageContext } from "../../../../context/LanguageContext"

const drawerWidth = 240;
const useStyles = makeStyles((theme: Theme) => createStyles({
    root: {
        color: palette.text.primary
    },
    small: {
        textTransform: 'uppercase',
        backgroundColor: theme.palette.primary.main,
        color: palette.primary.contrastText
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: 450,
        })
    },
    appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: 400,
        }),
    },
    menuButton: {
        marginRight: 36,
    },
    hide: {
        display: 'none',
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
        whiteSpace: 'nowrap',

    },
    drawerOpen: {
        width: drawerWidth,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: 400,
        }),
        overflowX: 'hidden'
    },
    drawerClose: {
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: 450,
        }),
        overflowX: 'hidden',
        width: theme.spacing(7) + 1,
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing(9) + 1,
        },
    },
    toolbar: {
        //display: 'flex',
        //flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: theme.spacing(0, 1),
        ...theme.mixins.toolbar,
    },
    toolbarItem: {
        display: 'flex',
        alignSelf: 'center',
        justifySelf: 'center',
        padding: theme.spacing(0, 1),
        ...theme.mixins.toolbar,
    },
    bottomPush: {
        position: "fixed",
        bottom: 0,
        textAlign: "center",
        paddingBottom: 10,
        ...theme.mixins.toolbar,
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(2),
    },
    nested: {
        paddingLeft: theme.spacing(4),
    },
    doubleNested: {
        paddingLeft: theme.spacing(8),
    }

}));

const TopBar = (props: any) => {
    const { openMobile, onMobileClose, className, ...rest } = props;
    const classes = useStyles();
    const userName = getUserName()
    let authController: AuthenticationController;
    const notificationsRef = useRef(null);
    const [notifications, setNotifications] = useState([]);
    const [accountSettingsExpanded, setAccountSettingsExpanded] = useState(false);
    const localization = React.useContext(LanguageContext);

    authController = new AuthenticationController(props.dataFetcher);

    function handleLogoutClick() {
        authController.logOut();
    }

    function handleAccountSettingsAction() {
        setAccountSettingsExpanded(!accountSettingsExpanded)
    }

    let role = getUserRole();
    let ldapUser = isLdapUser();

    let TQO = "none";
    let AS1 = "none";
    let AS2 = "none";

    if(role.includes("ADMIN") || role.includes("MANAGE")){
        TQO = "flex";
        AS1 = "flex";
        AS2 = "none";
    }
    return (
        <Fragment>
            <AppBar color="secondary"
                position="fixed"
                elevation={0}
                style={{ boxShadow: '0 1px 4px rgb(0 21 41 / 8%)' }}
                className={clsx(classes.appBar, {
                    [classes.appBarShift]: props.drawerOpen,
                })}>

                <Toolbar style={{ padding: '0 15px' }} className={classes.toolbar} >

                    <div style={{ flexGrow: 1 }} className={clsx(classes.menuButton, {
                        [classes.hide]: props.drawerOpen,
                    })} >
                        <RouterLink to={"/spaces"}>
                            <img style={{padding: '0', cursor: 'pointer'}} src="/img/pds_logo_short.png" alt="PDS logo" height="25"/>
                        </RouterLink>
                    </div>

                    {(props.pathname !== '/spaces' && props.pathname !== '/') && props.pdfExport && <div style={{margin: '5px'}}>
                        <PDFButton config={props.config} dataFetcher={props.dataFetcher} line={props.line} modelKeyword={props.modelKeyword}/>
                    </div>}

                    {(props.pathname !== '/spaces' && props.pathname !== '/') && <div style={{margin: '5px'}}>
                        <SelectSpaceButton/>
                    </div>}

                    <div style={{margin: '5px'}}>
                        <SelectLanguageButton />
                    </div>

                    <Chip  icon={<AccountCircleIcon style={{color: palette.primary.contrastText}}/>}
                           className={classes.small}
                           label={userName}
                           onMouseEnter={props.handleNotificationsOpen}
                           onClick={props.handleNotificationsOpen}
                           ref={notificationsRef}
                           style={{margin: '5px'}}
                    />

                </Toolbar>
            </AppBar>
            <NotificationsPopover
                anchorel={notificationsRef.current}
                notifications={notifications}
                //onClose={handleNotificationsClose}
                onMouseEnter={props.handleNotificationsOpen}
                onMouseLeave={props.handleNotificationsClose}
                open={props.openNotifications}
                child={
                    <List>
                        {/*{(role.includes('CHARTS') || role.includes("ADMIN")) &&*/}
                        {/*    <ListItem button component={RouterLink} to={'/konfigurierbares'}>*/}
                        {/*        <ListItemIcon>*/}
                        {/*            <EqualizerIcon style={{ color: '#00315B' }} fontSize="small" />*/}
                        {/*        </ListItemIcon>*/}
                        {/*        <ListItemText primary={localization.translations.common.my_dashboard}  />*/}
                        {/*    </ListItem>*/}
                        {/*}*/}
                        {/*<Divider />*/}
                        <ListItem style={{ display: AS1 }} button onClick={handleAccountSettingsAction}>
                            <ListItemIcon>
                                <SettingsIcon style={{ color: '#00315B' }} fontSize="small" />
                            </ListItemIcon>
                            <ListItemText style={{ whiteSpace: 'nowrap' }} primary={localization.translations.common.account_settings} />
                            {accountSettingsExpanded ?
                                <ExpandLess style={{ color: '#00315B' }} fontSize="small" />
                                :
                                <ExpandMore style={{ color: '#00315B' }} fontSize="small" />}
                        </ListItem>
                        <Collapse in={accountSettingsExpanded} timeout="auto" unmountOnExit>
                            <ListItem className={classes.nested} button id={"add_user"}
                                component={RouterLink} to={'/add_user'}
                            >
                                <ListItemIcon>
                                    <PersonAddIcon style={{ color: '#00315B' }} fontSize="small" />
                                </ListItemIcon>
                                <ListItemText primary={localization.translations.common.add_user} />
                            </ListItem>
                            <ListItem className={classes.nested} button id={"del_user"}
                                component={RouterLink} to={'/delete_user'}>
                                <ListItemIcon>
                                    <PersonAddDisabledIcon style={{ color: '#00315B' }} fontSize="small" />
                                </ListItemIcon>
                                <ListItemText primary={localization.translations.user.delete}  />
                            </ListItem>
                            {ldapUser === false ?
                                <ListItem className={classes.nested} button
                                    component={RouterLink} to={'/account_settings'}>
                                    <ListItemIcon>
                                        <LockIcon style={{ color: '#00315B' }} fontSize="small" />
                                    </ListItemIcon>
                                    <ListItemText primary={localization.translations.user.change_password} />
                                </ListItem>
                                :
                                ''}
                            <ListItem className={classes.nested} button
                                component={RouterLink} to={'/roles_settings'}
                            >
                                <ListItemIcon>
                                    <PeopleIcon style={{ color: '#00315B' }} fontSize="small" />

                                </ListItemIcon>
                                <ListItemText primary={localization.translations.user.role_settings} />
                            </ListItem>
                        </Collapse>
                        <Divider />
                        {ldapUser === false &&
                            <React.Fragment>
                                <ListItem style={{ display: AS2 }} button id={"change_password"}
                                    component={RouterLink} to={'/account_settings'}>
                                    <ListItemIcon>
                                        <SettingsIcon style={{ color: '#00315B' }} fontSize="small" />
                                    </ListItemIcon>
                                    <ListItemText primary={localization.translations.common.account_settings} />
                                </ListItem>
                                <Divider style={{ display: AS2 }} />
                            </React.Fragment>
                        }
                        {role.includes("MANAGE") &&
                            <ListItem button
                                component={RouterLink} to={'/roles_settings'}>
                                <ListItemIcon>
                                    <PeopleIcon style={{ color: '#00315B' }} fontSize="small" />
                                </ListItemIcon>
                                <ListItemText primary={localization.translations.user.role_settings} />
                            </ListItem>
                        }
                        {(role.includes("ADMIN") || role.includes("EMAIL_CONFIG")) &&
                            <ListItem button
                                component={RouterLink} to={'/email'}>
                                <ListItemIcon>
                                    <EmailIcon style={{ color: '#00315B' }} fontSize="small" />
                                </ListItemIcon>
                                <ListItemText primary={localization.translations.common.email_settings} />
                            </ListItem>
                        }
                        <Divider />
                        <ListItem button component={RouterLink} to={'/about'}
                        >
                            <ListItemIcon>
                                <InfoIcon style={{ color: '#00315B' }} fontSize="small" />
                            </ListItemIcon>
                            <ListItemText primary={localization.translations.common.about}
                            />
                        </ListItem>
                        <Divider />
                        <ListItem button onClick={handleLogoutClick}>
                            <ListItemIcon>
                                <PowerSettingsNewIcon style={{ color: colors.red[900], }} fontSize="small" />
                            </ListItemIcon>
                            <ListItemText primary={localization.translations.common.log_out} />
                        </ListItem>
                    </List>
                }
            />
        </Fragment>
    );
};

export default TopBar;

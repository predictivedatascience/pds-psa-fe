import React from 'react';
import {renderRoutes} from "react-router-config";

const Container = (props: any) =>{
    return (
        <React.Fragment>
            {renderRoutes(props.routes, props)}
        </React.Fragment>
    )
}
/**
 * Forcing this to be false to avoid re-render when Drawer/Popover is opened/closed in Dashboard.tsx
 */
export default React.memo(Container, (prevState, nextState) => {return true})

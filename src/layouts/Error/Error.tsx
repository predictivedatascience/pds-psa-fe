import React, { Suspense }  from 'react';
import { makeStyles } from '@material-ui/styles';
import { renderRoutes } from 'react-router-config';
import {LinearProgress} from "@material-ui/core";

const useStyles = makeStyles(() => ({
    root: {
        height: '100%'
    }
}));

const Error = (props: any) => {
    const classes = useStyles();

    return (
        <main className={classes.root}>
            <Suspense fallback={<LinearProgress />}>
                {renderRoutes(props.routes)}
            </Suspense>
        </main>
    );
};


export default Error;

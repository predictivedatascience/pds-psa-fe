import React, { Fragment, Suspense } from 'react';
import { makeStyles } from '@material-ui/styles';
import {renderRoutes} from "react-router-config";

const useStyles = makeStyles(theme => ({
    content: {
        height: '100%',
    }
}));

const AuthLayout = (props:any) => {
    const classes = useStyles();

    return (
        <Fragment>
            <main className={classes.content}>
                {renderRoutes(props.routes, props)}
            </main>
        </Fragment>
    );
};

export default AuthLayout;

import { colors } from '@material-ui/core';
import {indigo} from "@material-ui/core/colors";

const white = '#FFFFFF';
const black = '#000000';
const VWblue = '#00315B';

export default {
    black,
    white,
    VWblue,
    primary: {
        contrastText: white,
        dark: colors.indigo[900],
        main: '#00315B',
        light: colors.indigo[100]
    },
    secondary: {
        main: '#fff',
    },
    /*primary: {
        contrastText: white,
        dark: colors.indigo[900],
        main: colors.indigo[500],
        light: colors.indigo[100]
    },
    secondary: {
        contrastText: white,
        dark: colors.blue[900],
        main: colors.blue['A400'],
        light: colors.blue['A400']
    },*/
    error: {
        contrastText: white,
        dark: colors.red[900],
        main: colors.red[600],
        light: colors.red[400]
    },
    text: {
        primary: colors.grey[900],
        secondary: colors.grey[500],
        textPrimary: colors.grey[100],
        textSecondary: colors.indigo[800],
        //link: colors.blue[600],
        link: indigo
    },
    //link: colors.blue[800],
    link: indigo,
    //icon: colors.blueGrey[600],
    icon: indigo,
    background: {
        default: '#F4F6F8',
        paper: white
    },
    divider: colors.grey[200]
};

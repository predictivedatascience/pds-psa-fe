import {createMuiTheme} from "@material-ui/core";
import palette from "./palette";
import typography from "./typography";
import overrides from './overrides/index'

export const theme = createMuiTheme({
    /*palette: {
        primary: indigo,
        secondary: {
            main: '#fff',
        },
        background: {
            default: '#F0F2F5'
        }
    },*/
    palette,
    typography,
    overrides,
    props: {
        MuiButton: {
            disableElevation: true,
        },
    },
});

import palette from '../palette';

export default {
    root: {
       position: "relative",
            '& $notchedOutline': {
            borderColor: palette.VWblue,
        },
        '&:hover:not($disabled):not($focused):not($error) $notchedOutline': {
            borderColor: palette.VWblue,
                // Reset on touch devices, it doesn't add specificity
                '@media (hover: none)': {
                borderColor: palette.VWblue,
            },
        },
        '&$focused $notchedOutline': {
            borderColor: palette.VWblue,
                borderWidth: 1,
        },
    },
    notchedOutline: {
        borderColor: 'rgba(0,0,0,0.15)'
    }
}
export default {
    root: {
        borderRadius: '2px',
        '&:active': {
            boxShadow: 'inset 0px 4px 4px rgba(0, 40, 56, 0.4)',
        },
    },
}
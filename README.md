
# PDS Reports

PDS Reports is an application to present visualizations of data originating from IoT devices.

### Currently integrated chart libraries:
- [Nivo](https://nivo.rocks/)
- [Elastic Charts](https://github.com/elastic/elastic-charts)
- [Recharts](https://recharts.org/en-US/)

### Currently supported charts:
- bar chart
- line chart
- pie chart
- gauge chart
- table

## Getting Started
These instructions will guide you through the process of installation and configuration of the app.
### Prerequisites
1. Installed [NodeJS](https://nodejs.org/en/)
2. Installed [npm](https://www.npmjs.com/)


### Installing
1. Install the required dependencies with:
```
npm install
```
2. Copy content of `/src/view/charts/index.d.ts` to `node_modules/@elastic/charts/dist/utils/themes/index.d.ts`. This adds the threshold line feature to bar charts.

3. Copy content of `src/view/charts/materialTable/m-table-group-row.js` to `node_modules/material-table/dist/components/m-table-group-row.js`
4. Copy content of `src/view/charts/materialTable/index.d.ts` to `node_modules/material-table/types/index.d.ts`. 
This add badge to group count for each row.
5. Copy content of `src/view/charts/materialTable/prop-types.js` to `node_modules/material-table/dist/prop-types.js`. 
6. Copy content of `src/view/charts/materialTable/material-table.js` to `node_modules/material-table/dist/material-table.js`. 

### Configuration
The configuration of the app is implemented via environment variables. These are automatically loaded from `.env.*` files. You have to override the following fields:
* REACT_APP_BACKEND_URL: an URL to backend

## Running the tests
```
TODO
```

## Running
### Prerequisites
1. Running PDS Dashboard WS. You can find the instructions how to run the PDS Dashboard WS in the following [repository](https://bitbucket.org/predictivedatascience/pds-data-dashboards-ws).
### Start
1. Execute the command below.
```
npm run start
```
## Building
0. See [https://create-react-app.dev/docs/deployment/](https://create-react-app.dev/docs/deployment/)
1. Create a build of the app. The following command generates a `/build` folder, containing the built app.
```
npm run build
```
2. The build app can be deployed to the server and executed via:
```
serve -s build -l PORT_NUM
```
Tutorial to deploy the app via IIS (no need to use the command 2) if you are deploying to IIS):
https://stackoverflow.com/questions/51823797/create-react-app-on-iis-10

## Built With

* [NodeJS](https://nodejs.org/en/) - Runtime Environment
* [NPM](https://www.npmjs.com/) - Dependency Management
* [Typescript](https://www.typescriptlang.org/) - Programming Language
* [React](https://reactjs.org/) - Library for building UI
* [React Material-UI](https://material-ui.com/) - React Components for Material UI

## License
TODO

## MVC
The source code of the application is organized according to MVC - commonly used software design pattern to layout projects in more effective way.  The goal of this pattern is to split a large application into specific sections that all have their own purpose.  MVC stands for Model, View and Controller.
1. **Model** - Model represents an object carrying data. It can also have logic to update controller if its data changes. Model defines essential terms of your app.
2. **View** - View code contains all functions which directly interact with the user. It is the visualization of the data that model contains. 
3. **Controller** - Controller acts on both model and view. It controls the data flow into model object and updates the view whenever data changes. It keeps view and model separate.

![PDS PATH](/PDS/pds-data-dashboard/PATH.jpg)

### How does MVC work?
Based on user request, the server will send all requested information to a specific controller. Controller needs to handle the request flow and tell the rest of the server what to do.  Firstly, controller asks the model and backend for information because it shouldn’t contact database directly itself. After obtaining the request data from BE and model, it only cares about interacting with data. Afterwards it pushes the data to view. View is only concerned with present the information, but not with the final presentation to the user. It will render the information into a HTML, that can be used by the browser. Model and view never interact with each other, any interaction they made is done only with assistance of controller.

![MVC RES AND REQ](/PDS/pds-data-dashboard/MVC.jpg)